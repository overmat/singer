<?php
# No need for the template engine
define( 'WP_USE_THEMES', false );
# Load WordPress Core
// Assuming we're in a subdir: "~/wp-content/plugins/current_dir"
require_once( 'wp-load.php' );
use Roots\Sage\Titles;
global $wpdb, $user_ID;

//die();

if($_POST['action'] == "tg_pwd_reset"){
    if ( !wp_verify_nonce( $_POST['tg_pwd_nonce'], "tg_pwd_nonce")) {
        exit("No trick please");
    }
    if(empty($_POST['user_input'])) {
        echo "<div class='error'>Merci de renseigner votre adresse email.</div>";
        die();
    }
    //We shall SQL escape the input
    $user_input = $wpdb->escape(trim($_POST['user_input']));

    if ( strpos($user_input, '@') ) {
        $user_data = get_user_by('email', $user_input);
        if(empty($user_data)) { //delete the condition $user_data->caps[administrator] == 1, if you want to allow password reset for admins also
            echo "<div class='error'>Nous ne trouvons pas cette adresse email dans notre base.</div>";
            die();
        }
    }
    else {
        $user_data = get_userdatabylogin($user_input);
        if(empty($user_data) || $user_data->caps[administrator] == 1) { //delete the condition $user_data->caps[administrator] == 1, if you want to allow password reset for admins also
            echo "<div class='error'>Nous ne trouvons pas cette adresse email dans notre base.</div>";
            die();
        }
    }

    $user_login = $user_data->user_login;
    $user_email = $user_data->user_email;

    $key = $wpdb->get_var($wpdb->prepare("SELECT user_activation_key FROM $wpdb->users WHERE user_login = %s", $user_login));
    if(empty($key)) {
        //generate reset key
        $key = wp_generate_password(20, false);
        $wpdb->update($wpdb->users, array('user_activation_key' => $key), array('user_login' => $user_login));
    }

    $body = '
                                <table>
                    <tr>
                        <td>
                            <img src="https://www.singerfrance.com/wp-content/themes/singer/assets/images/logo-mail-200.png" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Bonjour,
                            <br /><br />
                            <p>
                                Une demande de nouveau mot de passe a été effectué sur notre site <a href="https://www.singerfrance.com">singerfrance.com</a> a bien été prise en compte.
                            </p>
                            <p>Afin de générer un nouveau mot de passe pour votre compte, merci de cliquer sur  ce lien : <a href="'.tg_validate_url() .'action=reset_pwd&key='.$key.'&login=' .$user_login.'">'.tg_validate_url() .'action=reset_pwd&key='.$key.'&login=' . $user_login.'</a></p>
                            <p>A très bientôt sur <a href="http://www.singerfrance.com">singerfrance.com</a> !</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="margin-top:30px;">Suivez nos actualités et nos offres promotionnelles !</p>
                            <a href="https://www.facebook.com/SingerFR/"><img src="http://www.singerfrance.com/wp-content/themes/singer/assets/images/ico-fb.jpg" /></a> <a href="https://www.instagram.com/singerfrance/"><img src="http://www.singerfrance.com/wp-content/themes/singer/assets/images/icon-insta.jpg" /></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="margin:O;">---------------</p>
                            Cet e-mail vous est envoyé automatiquement du serveur <a href="https://www.singerfrance.com">singerfrance.com</a>, merci de ne pas y répondre.
                        </td>
                    </tr>
                </table>
                ';
    if ( $body && !wp_mail($user_email, 'Demande de nouveau mot de passe sur Singerfrance.com', $body) ) {
        echo "<div class='error'>Impossible d'envoyer l'email.</div>";
        die();
    }
    else {
        echo "<div class='success'>Nous venons de vous envoyer une mail avec les instructions pour obtenir votre nouveau mot de passe.</div>";
        die();
    }

}