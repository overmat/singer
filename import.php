<?php

# No need for the template engine
define( 'WP_USE_THEMES', false );

require_once( 'wp-load.php' );

// Génération d'une chaine aléatoire
function chaine_aleatoire($nb_car, $chaine = 'azertyuiopqsdfghjklmwxcvbn123456789')
{
    $nb_lettres = strlen($chaine) - 1;
    $generation = '';
    for($i=0; $i < $nb_car; $i++)
    {
        $pos = mt_rand(0, $nb_lettres);
        $car = $chaine[$pos];
        $generation .= $car;
    }
    return $generation;
}


    $row = 0;
    if (($handle = fopen("User.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

            if (is_email($data[4]) && !email_exists($data[4])) {
                $password = chaine_aleatoire(8);
                $userdata = array(
                    'user_login' => esc_attr($data[4]),
                    'user_email' => esc_attr($data[4]),
                    'user_pass' => esc_attr($password),
                    'first_name' => esc_attr($data[19]),
                    'last_name' => esc_attr($data[18]),
                );

                $register_user = wp_insert_user($userdata);

                if (!is_wp_error($register_user)) {
                    update_user_meta($register_user, 'wpcf-user-phone', esc_attr($data[16]));
                    update_user_meta($register_user, 'wpcf-user-city', esc_attr($data[22]));
                    update_user_meta($register_user, 'wpcf-user-address', esc_attr($data[20]));
                    update_user_meta($register_user, 'wpcf-user-zipcode', esc_attr($data[21]));
                    update_user_meta($register_user, 'wpcf-optin', 0);

                    $csv[] = array($data[4], $password, $data[19], $data[18]);
                }
            }
            $row++;
            var_dump($row);
        }

        fclose($handle);
    }

    $fp = fopen('password.csv', 'w');

    foreach ($csv as $fields) {
        fputcsv($fp, $fields);
    }

    fclose($fp);
