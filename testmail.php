<?php

# No need for the template engine
define( 'WP_USE_THEMES', false );

require_once( 'wp-load.php' );

$to = 'matthieu@full-adgency.com';
$subject = 'Confirmation de création de compte sur singerfrance.com';
$body = '
<table>
    <tr>
        <td>
            <img style="width:200px;margin-bottom:20px;" src="http://www.singerfrance.com/wp-content/themes/singer/assets/images/logo.jpg" />
        </td>
    </tr>
    <tr>
        <td>
            Bonjour Matthieu Birot,
            <br />
            <p>
                Nous vous remercions pour l\'enregistrement de votre numéro de série sur notre site <a href="http://www.singerfrance.com">singerfrance.com</a>. Vous bénéficiez dès aujourd’hui d\'une garantie supplémentaire de 6 mois.
            </p>
            <p>
                <ul>
                    <li>Votre modèle de machine : birot.matthieu@full-adgency.com</li>
                    <li>Votre de numéro série  : ptolemee</li>
                </ul>
            </p>
            <p>A très bientôt sur <a href="http://www.singerfrance.com">singerfrance.com</a> !</p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin-top:30px;">Suivez nos actualités et nos offres promotionnelles !</p>
            <a href=""><img src="http://www.singerfrance.com/wp-content/themes/singer/assets/images/ico-fb.jpg" /></a> <a href=""><img src="http://www.singerfrance.com/wp-content/themes/singer/assets/images/icon-insta.jpg" /></a>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin:O;">---------------</p>
            Cet e-mail vous est envoyé automatiquement du serveur <a href="http://www.singerfrance.com">singerfrance.com</a>, merci de ne pas y répondre.
        </td>
    </tr>
</table>
';

wp_mail( $to, $subject, $body);