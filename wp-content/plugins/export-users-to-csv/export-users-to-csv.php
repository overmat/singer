<?php
/*
Plugin Name: Export Users to CSV
Plugin URI: http://wordpress.org/extend/plugins/export-users-to-csv/
Description: Export Users data and metadata to a csv file.
Version: 1.0.1
Author: Matt Cromwell
Author URI: https://www.mattcromwell.com/products/export-users-to-csv
License: GPL2
Text Domain: export-users-to-csv
*/
/*  Copyright 2017  Matt Cromwell  (http://github.com/mathetos/export-users-to-csv)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

load_plugin_textdomain( 'export-users-to-csv', false, basename( dirname( __FILE__ ) ) . '/languages' );

/**
 * Main plugin class
 *
 * @since 0.1
 **/
class PP_EU_Export_Users {

	/**
	 * Class contructor
	 *
	 * @since 0.1
	 **/
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'add_admin_pages' ) );
		add_action( 'init', array( $this, 'generate_csv' ) );
		add_filter( 'pp_eu_exclude_data', array( $this, 'exclude_data' ) );
	}

	/**
	 * Add administration menus
	 *
	 * @since 0.1
	 **/
	public function add_admin_pages() {
		add_users_page( __( 'Export to CSV', 'export-users-to-csv' ), __( 'Export to CSV', 'export-users-to-csv' ), 'list_users', 'export-users-to-csv', array( $this, 'users_page' ) );
	}

	/**
	 * Process content of CSV file
	 *
	 * @since 0.1
	 **/
	public function generate_csv() {
		if ( isset( $_POST['_wpnonce-pp-eu-export-users-users-page_export'] ) ) {
			check_admin_referer( 'pp-eu-export-users-users-page_export', '_wpnonce-pp-eu-export-users-users-page_export' );

			$args = array(
				'fields' => 'all_with_meta',
			);

			add_action( 'pre_user_query', array( $this, 'pre_user_query' ) );
			$users = get_users( $args );
			remove_action( 'pre_user_query', array( $this, 'pre_user_query' ) );

			if ( ! $users ) {
				$referer = add_query_arg( 'error', 'empty', wp_get_referer() );
				wp_redirect( $referer );
				exit;
			}

			$sitename = sanitize_key( get_bloginfo( 'name' ) );
			if ( ! empty( $sitename ) )
				$sitename .= '.';
			$filename = $sitename . 'users.' . date( 'Y-m-d-H-i-s' ) . '.csv';


			header( 'Content-Description: File Transfer' );
			header( 'Content-Disposition: attachment; filename=' . $filename );
			header( 'Content-Type: text/csv; charset=' . get_option( 'blog_charset' ), true );


			$exclude_data = apply_filters( 'pp_eu_exclude_data', array() );

			global $wpdb;

			$data_keys = array(
				'ID', 'user_email', 'user_registered'
			);

			$meta_keys = array(
                'first_name',
                'last_name',
                'wpcf-user-city',
                'wpcf-user-address',
                'wpcf-user-zipcode',
                'wpcf-user-phone',
                'wpcf-optin'
            );

			$fields = array_merge( $data_keys, $meta_keys );

			$headers = array();
			foreach ( $fields as $key => $field ) {
				if ( in_array( $field, $exclude_data ) )
					unset( $fields[$key] );
				else
					$headers[] = '"' . strtolower( $field ) . '"';
			}

			echo implode( '|', $headers ) . "\n";

			foreach ( $users as $user ) {
                $display = true;
                $t = 'wpcf-optin';

                if ($_POST['optin'] == '0' && $user->{$t} == '1') {
                    $display = false;
                } elseif ($_POST['optin'] == '1' && $user->{$t} != '1') {
                    $display = false;
                } else {
                    $display = true;
                }

                if($user->{$t} == '' || $user->{$t} == false || $user->{$t} == null || $user->{$t} == '0') {
                	$user->{$t} = '0';
                } else {
                	$user->{$t} = '1';
                }

                $zip = 'wpcf-user-zipcode';

                if($_POST['zipcode'] != '') {
                    if($_POST['zipcode'] !== $user->{$zip}) {
                        $display = false;
                    }
                }
                $id = 'id';
                $extensions = $wpdb->get_results('SELECT * FROM singer_extension_garantie WHERE id_user = '.$user->{$id});

                if(empty($extensions) && $_POST['num'] == '1') {
                    $display = false;
                }

                if(empty($extensions) && $_POST['num'] == '1') {
                    $display = false;
                }

                if($display) {
                    $data = array();
                    foreach ($fields as $field) {

                        $value = isset($user->{$field}) ? $user->{$field} : '';
                        $value = is_array($value) ? serialize($value) : $value;
                        $value = utf8_decode($value);
                        $value = str_replace('<br />', ' ',$value);
                        $value = str_replace('&#039;', "'", $value);
                        $value = str_replace(array("\r\n", "\n\r", "\n", "\r"), ',', $value);

                        $data[] = '"' . str_replace('"', '', $value) . '"';
                    }

                    echo implode('|', $data) . "\n";
                }
			}

			exit;
		}
	}

	/**
	 * Content of the settings page
	 *
	 * @since 0.1
	 **/
	public function users_page() {
		if ( ! current_user_can( 'list_users' ) )
			wp_die( __( 'You do not have sufficient permissions to access this page.', 'export-users-to-csv' ) );
?>

<div class="wrap">
	<h2><?php _e( 'Export users to a CSV file', 'export-users-to-csv' ); ?></h2>
	<?php
	if ( isset( $_GET['error'] ) ) {
		echo '<div class="updated"><p><strong>' . __( 'No user found.', 'export-users-to-csv' ) . '</strong></p></div>';
	}
	?>
	<form method="post" action="" enctype="multipart/form-data">
		<?php wp_nonce_field( 'pp-eu-export-users-users-page_export', '_wpnonce-pp-eu-export-users-users-page_export' ); ?>
		<table class="form-table">
			<tr valign="top">
				<th scope="row"><label>Inscription</label></th>
				<td>
					De
					<input type="text" name="date_from" class="date-picker"/>
					A
					<input type="text" name="date_to" class="date-picker"/>
				</td>
			</tr>
            <tr>
                <th>
                    <label>Optin</label>
                </th>
                <td>
                    <select name="optin">
                        <option value="-1">Tous</option>
                        <option value="1">Avec optin</option>
                        <option value="0">Sans optin</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th>
                    <label>Numéro de série</label>
                </th>
                <td>
                    <select name="num">
                        <option value="-1">Tous</option>
                        <option value="1">Avec</option>
                        <option value="0">Sans</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th>
                    <label>Code postal</label>
                </th>
                <td>
                    <input type="text" name="zipcode" />
                </td>
            </tr>
		</table>
		<p class="submit">
			<input type="hidden" name="_wp_http_referer" value="<?php echo $_SERVER['REQUEST_URI'] ?>" />
			<input type="submit" class="button-primary" value="<?php _e( 'Export', 'export-users-to-csv' ); ?>" />
		</p>
	</form>
<?php
	}

	public function exclude_data() {
		$exclude = array( 'user_pass', 'user_activation_key' );

		return $exclude;
	}

	public function pre_user_query( $user_search ) {
		global $wpdb;

		$where = '';

		if ( ! empty( $_POST['date_from'] ) )
			$where .= $wpdb->prepare( " AND $wpdb->users.user_registered >= %s", $_POST['date_from'] );

		if ( ! empty( $_POST['date_to'] ) )
			$where .= $wpdb->prepare( " AND $wpdb->users.user_registered < %s", $_POST['date_to'] );

		if ( ! empty( $where ) )
			$user_search->query_where = str_replace( 'WHERE 1=1', "WHERE 1=1$where", $user_search->query_where );

		return $user_search;
	}

	private function export_date_options() {
		global $wpdb, $wp_locale;

		$months = $wpdb->get_results( "
			SELECT DISTINCT YEAR( user_registered ) AS year, MONTH( user_registered ) AS month
			FROM $wpdb->users
			ORDER BY user_registered DESC
		" );

		$month_count = count( $months );
		if ( !$month_count || ( 1 == $month_count && 0 == $months[0]->month ) )
			return;

		foreach ( $months as $date ) {
			if ( 0 == $date->year )
				continue;

			$month = zeroise( $date->month, 2 );
			echo '<option value="' . $date->year . '-' . $month . '">' . $wp_locale->get_month( $month ) . ' ' . $date->year . '</option>';
		}
	}
}

new PP_EU_Export_Users;
