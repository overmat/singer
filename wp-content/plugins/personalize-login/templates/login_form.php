<div class="login-form-container">
	<?php if ( $attributes['show_title'] ) : ?>
		<h2><?php _e( 'Se connecter', 'personalize-login' ); ?></h2>
	<?php endif; ?>
    <div class="row">
        <div class="col-lg-4 col-md-12 col-sm-12">
            <!-- Show errors if there are any -->
            <?php if ( count( $attributes['errors'] ) > 0 ) : ?>
                <?php foreach ( $attributes['errors'] as $error ) : ?>
                    <div class="error-account">
                        <?php echo $error; ?>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

            <form name="loginform" id="loginform" action="<?php echo esc_url( site_url( 'wp-login.php', 'login_post' ) ); ?>" method="post">

                <div class="form-group">
                    <input type="text" name="log" id="user_login" class="form-control" value="" placeholder="Adresse email">
                </div>
                <div class="form-group">
                    <input type="password" name="pwd" id="user_pass" class="form-control" value="" placeholder="Mot de passe">
                </div>
                <div class="login-remember"><label><input name="rememberme" type="checkbox" id="rememberme" value="forever"> Se souvenir de moi</label></div>
                <div class="login-submit">
                    <input type="submit" name="wp-submit" id="wp-submit" class="btn btn-primary" value="Je me connecte">
                    <input type="hidden" name="redirect_to" value="<?php echo $attributes['redirect']; ?>">
                </div>
                <div class="lost-password">
                    <a href="<?php echo wp_lostpassword_url();?>">Mot de passe oublié ?</a>
                </div>
                <div class="lost-password-text">
                Avec l'arrivée du nouveau site il est possible que vous deviez réinitialiser votre mot de passe. Pour réinitialiser votre mot de passe, cliquez sur mot de passe oublié.
                </div>
            </form>

        </div>
    </div>
</div>
