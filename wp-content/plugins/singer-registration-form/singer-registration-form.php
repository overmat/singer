<?php

/*
  Plugin Name: Singer Registration Form
  Description: Formulaire inscription
  Version: 1.0
  Author: Matthieu Birot
 */


class Singer_registration_form
{

    private $username;
    private $email;
    private $password;
    private $first_name;
    private $last_name;

    private $password_confirm;
    private $phone;
    private $city;
    private $address;
    private $zipcode;

    private $model;
    private $serie_number;
    private $serie_number_confirm;
    private $lieu_achat;
    private $optin = false;

    private $validation = false;
    private $userError = false;

    function __construct()
    {
        add_shortcode('singer_registration_form', array($this, 'shortcode'));
        add_shortcode('singer_my_account', array($this, 'my_account_shortcode'));
        add_shortcode('singer_extension_garantie_form', array($this, 'extension_garantie_shortcode'));
        add_shortcode('singer_change_password_form', array($this, 'password_shortcode'));
        add_shortcode('singer_edit_informations_form', array($this, 'edit_informations_shortcode'));
        add_action('init',array($this, 'manage_register_form'));
        add_action('init',array($this, 'manage_extension_form'));
        add_action('init',array($this, 'manage_password_form'));
        add_action('init',array($this, 'manage_edit_informations_form'));
    }

    public function edit_informations_form()
    {
        $userData = get_userdata(get_current_user_id());

        if (is_wp_error($this->validation)) {
            echo '<div class="row"><div class="col-lg-6 col-lg-push-3"><div class="error-account">';
            echo '<strong>' . $this->validation->get_error_message() . '</strong>';
            echo '</div></div></div>';
        }

        if(is_wp_error($this->userError)) {
            echo '<div class="row"><div class="col-lg-6 col-lg-push-3"><div class="error-account">';
            echo '<strong>' . $this->userError->get_error_message() . '</strong>';
            echo '</div></div></div>';
        }

        ?>

        <form method="post" autocomplete="off" action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>">
            <div class="login-form account">
                <div class="row">
                    <div class="col-lg-6 col-lg-push-3">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Informations de connexion</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input name="reg_email" type="email" class="form-control login-field"
                                           value="<?php echo(isset($_POST['reg_email']) ? $_POST['reg_email'] : $userData->user_email); ?>"
                                           placeholder="Adresse E-mail*" id="reg-email" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input name="reg_lname" type="text" class="form-control login-field"
                                           value="<?php echo(isset($_POST['reg_lname']) ? $_POST['reg_lname'] : get_user_meta(get_current_user_id(), 'last_name', true)); ?>"
                                           placeholder="Nom*" id="reg-lname"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input name="reg_fname" type="text" class="form-control login-field"
                                           value="<?php echo(isset($_POST['reg_fname']) ? $_POST['reg_fname'] : get_user_meta(get_current_user_id(), 'first_name', true)); ?>"
                                           placeholder="Prénom*" id="reg-fname"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input name="reg_phone" type="text" class="form-control login-field"
                                           value="<?php echo(isset($_POST['reg_phone']) ? $_POST['reg_phone'] : get_user_meta(get_current_user_id(), 'wpcf-user-phone', true)); ?>"
                                           placeholder="Téléphone*" id="reg-phone"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input name="reg_address" type="text" class="form-control login-field"
                                           value="<?php echo(isset($_POST['reg_address']) ? $_POST['reg_address'] : get_user_meta(get_current_user_id(), 'wpcf-user-address', true)); ?>"
                                           placeholder="Adresse*" id="re-address"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input name="reg_city" type="text" class="form-control login-field"
                                           value="<?php echo(isset($_POST['reg_city']) ? $_POST['reg_city'] : get_user_meta(get_current_user_id(), 'wpcf-user-city', true)); ?>"
                                           placeholder="Ville*" id="reg-city"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input name="reg_zipcode" type="text" class="form-control login-field"
                                           value="<?php echo(isset($_POST['reg_zipcode']) ? $_POST['reg_zipcode'] : get_user_meta(get_current_user_id(), 'wpcf-user-zipcode', true)); ?>"
                                           placeholder="Code postal*" id="reg_zipcode"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    *champs obligatoires
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-lg-push-3">
                                <input class="btn btn-primary btn-lg btn-block" type="submit" name="reg_submit_informations" value="Mettre à jour"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php
    }

    public function change_password_form() {

        if (is_wp_error($this->validation)) {
            echo '<div class="row"><div class="col-lg-6 col-lg-push-3"><div class="error-account">';
            echo '<strong>' . $this->validation->get_error_message() . '</strong>';
            echo '</div></div></div>';
        }
        ?>

        <div class="row">
            <div class="col-lg-6 col-lg-push-3">
                <form method="post" action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>">
                    <div class="form-group">
                        <input name="reg_password" type="password" class="form-control login-field"
                               value="<?php echo(isset($_POST['reg_password']) ? $_POST['reg_password'] : null); ?>"
                               placeholder="Nouveau mot de passe*" id="reg_serie_number"/>
                    </div>
                    <div class="form-group">
                        <input name="reg_password_confirm" type="password" class="form-control login-field"
                               value="<?php echo(isset($_POST['reg_password_confirm']) ? $_POST['reg_password_confirm'] : null); ?>"
                               placeholder="Confirmation*" id="reg_password_confirm"/>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-3">
                            <input class="btn btn-primary btn-lg btn-block" type="submit" name="reg_submit_password" value="Mettre à jour"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>


        <?php
    }

    public function extension_garantie_form() {
        $models = get_posts( array('post_type' => 'produit', 'posts_per_page' => -1));
        ?>
        <?php
        if (is_wp_error($this->validation)) {
            echo '<div class="row"><div class="col-lg-6 col-lg-push-3"><div class="error-account">';
            echo '<strong>' . $this->validation->get_error_message() . '</strong>';
            echo '</div></div></div>';
        }
        ?>
        <div class="row">
            <div class="col-lg-6 col-lg-push-3">
                <form method="post" autocomplete="off" action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>">
                    <div class="form-group">
                        <select name="reg_model" class="form-control">
                            <option value="--">Sélectionnez votre modèle</option>
                            <?php foreach ($models as $model):
                                $terms = wp_get_post_terms($model->ID, 'catalogue');
                                $metas = get_all_wp_terms_meta($terms[0]->term_id);

                                if($metas['garantie'][0] == 'checked'):
                            ?>
                                <option value="<?php echo $model->post_title; ?>"><?php echo $model->post_title; ?></option>
                            <?php endif; endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="reg_lieu" class="form-control">
                            <option value="--">Selectionnez le lieu d'achat</option>
                            <option value="Sur internet">Sur internet</option>
                            <option value="Grande distribution">Grande distribution</option>
                            <option value="Magasin Singer">Magasin Singer</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input name="reg_serie_number" type="text" class="form-control login-field"
                               value="<?php echo(isset($_POST['reg_serie_number']) ? $_POST['reg_serie_number'] : null); ?>"
                               placeholder="Numéro de série" id="reg_serie_number"/>
                    </div>
                    <div class="form-group">
                        <input name="reg_serie_number_confirm" type="text" class="form-control login-field"
                               value="<?php echo(isset($_POST['reg_serie_number_confirm']) ? $_POST['reg_serie_number_confirm'] : null); ?>"
                               placeholder="Numéro de série (confirmation)" id="reg_serie_number_confirm"/>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-3">
                            <input class="btn btn-primary btn-lg btn-block" type="submit" name="reg_submit_extension_garantie" value="Ajouter"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>


        <?php
    }

    public function registration_form()
    {
        $models = get_posts( array('post_type' => 'produit', 'posts_per_page' => -1));

        if (is_wp_error($this->validation)) {
            echo '<div class="row"><div class="col-lg-6 col-lg-push-3"><div class="error-account">';
            echo '<strong>' . $this->validation->get_error_message() . '</strong>';
            echo '</div></div></div>';
        }

        if(is_wp_error($this->userError)) {
            echo '<div class="row"><div class="col-lg-6 col-lg-push-3"><div class="error-account">';
            echo '<strong>' . $this->userError->get_error_message() . '</strong>';
            echo '</div></div></div>';
        }

        ?>

        <form method="post" autocomplete="off" action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>">
            <div style="display: none;">
                <input style="display:none" type="text" name="username"/>
                <input style="display:none" type="password" name="password"/>
                <input style="display:none" type="text" name="phone"/>
                <input style="display:none" type="text" name="company"/>
                <input style="display:none" type="text" name="number"/>
            </div>
            <div class="login-form account">
                <div class="row">
                    <div class="col-lg-6 col-lg-push-3">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Informations de connexion</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Adresse E-mail*</label>
                                    <input name="reg_email" type="email" class="form-control login-field"
                                           value="<?php echo(isset($_POST['reg_email']) ? $_POST['reg_email'] : null); ?>"
                                           placeholder="Adresse E-mail*" id="reg-email" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Mot de passe*</label>
                                    <input name="reg_password" type="password" class="form-control login-field"
                                           value="<?php echo(isset($_POST['reg_password']) ? $_POST['reg_password'] : null); ?>"
                                           placeholder="Mot de passe*" id="reg-pass" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Adresse E-mail (confirmation)*</label>
                                    <input name="reg_password_confirm" type="password" class="form-control login-field"
                                           value="<?php echo(isset($_POST['reg_password_confirm']) ? $_POST['reg_password_confirm'] : null); ?>"
                                           placeholder="Mot de passe (confirmation)*" id="reg-pass-confirm" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nom*</label>
                                    <input name="reg_lname" type="text" class="form-control login-field"
                                           value="<?php echo(isset($_POST['reg_lname']) ? $_POST['reg_lname'] : null); ?>"
                                           placeholder="Nom*" id="reg-lname"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Prénom*</label>
                                    <input name="reg_fname" type="text" class="form-control login-field"
                                           value="<?php echo(isset($_POST['reg_fname']) ? $_POST['reg_fname'] : null); ?>"
                                           placeholder="Prénom*" id="reg-fname"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Numéro de téléphone*</label>
                                    <input name="reg_phone" type="text" class="form-control login-field"
                                           value="<?php echo(isset($_POST['reg_phone']) ? $_POST['reg_phone'] : null); ?>"
                                           placeholder="Téléphone" id="reg-phone"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Adresse*</label>
                                    <input name="reg_address" type="text" class="form-control login-field"
                                           value="<?php echo(isset($_POST['reg_address']) ? $_POST['reg_address'] : null); ?>"
                                           placeholder="Adresse" id="re-address"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Ville*</label>
                                    <input name="reg_city" type="text" class="form-control login-field"
                                           value="<?php echo(isset($_POST['reg_city']) ? $_POST['reg_city'] : null); ?>"
                                           placeholder="Ville" id="reg-city"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Code postal*</label>
                                    <input name="reg_zipcode" type="text" class="form-control login-field"
                                           value="<?php echo(isset($_POST['reg_zipcode']) ? $_POST['reg_zipcode'] : null); ?>"
                                           placeholder="Code postal*" id="reg_zipcode"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label><input type="checkbox" name="optin" style="margin-right: 10px;">J’accepte de recevoir les actualités et promotions de la part de Singer</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="ext-title">Extension de garantie singer</h2>
                                <p>Vous devez d’abord vous inscrire en remplissant les cases ci-dessus pour bénéficier de votre extension de garantie.</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <select name="reg_model" class="form-control">
                                <label>Modèle</label>
                                <option value="--">Sélectionnez votre modèle</option>
                                <?php foreach ($models as $model):
                                    $terms = wp_get_post_terms($model->ID, 'catalogue');
                                    $metas = get_all_wp_terms_meta($terms[0]->term_id);

                                    if($metas['garantie'][0] == 'checked'):
                                ?>

                                    <option value="<?php echo $model->post_title; ?>"><?php echo $model->post_title; ?></option>

                                <?php endif; endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Lieu d'achat</label>
                            <select name="reg_lieu" class="form-control">
                                <option value="--">Selectionnez le lieu d'achat</option>
                                <option value="Sur internet">Sur internet</option>
                                <option value="Grande distribution">Grande distribution</option>
                                <option value="Magasin Singer">Magasin Singer</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Numéro de série</label>
                            <input name="reg_serie_number" type="text" class="form-control login-field"
                                   value="<?php echo(isset($_POST['reg_serie_number']) ? $_POST['reg_serie_number'] : null); ?>"
                                   placeholder="Numéro de série" id="reg_serie_number"/>
                        </div>
                        <div class="form-group">
                            <label>Numéro de série (confirmation)</label>
                            <input name="reg_serie_number_confirm" type="text" class="form-control login-field"
                                   value="<?php echo(isset($_POST['reg_serie_number_confirm']) ? $_POST['reg_serie_number_confirm'] : null); ?>"
                                   placeholder="Numéro de série (confirmation)" id="reg_serie_number_confirm"/>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    *champs obligatoires
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-lg-push-3">
                                <input class="btn btn-primary btn-lg btn-block" type="submit" name="reg_submit" value="S'inscrire"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    <?php
    }

    function validation()
    {

        if (
            empty($this->password) ||
            empty($this->first_name) ||
            empty($this->last_name) ||
            empty($this->zipcode) ||
            empty($this->password_confirm) ||
            empty($this->email)) {
            return new WP_Error('field', 'Des champs obligatoires ne sont pas renseignés');
        }

        $models = get_posts( array('post_type' => 'produit', 'posts_per_page' => -1));

        foreach ($models as $model) {
            $authorizedModels[] = $model->post_title;
        }


        if(($this->model !== '--') && (empty($this->serie_number) || empty($this->serie_number_confirm))) {
            return new WP_Error('model', 'Les champs pour obtenir l\'extension de garantie sont incomplets.');
        }

        if(!empty($this->serie_number) && ((empty($this->model) || $this->model == '--') || empty($this->serie_number_confirm))) {
            return new WP_Error('model', 'Les champs pour obtenir l\'extension de garantie sont incomplets.');
        }

        if(!empty($this->serie_number_confirm) && ((empty($this->model) || $this->model == '--') || empty($this->serie_number))) {
            return new WP_Error('model', 'Les champs pour obtenir l\'extension de garantie sont incomplets.');
        }

        if(!empty($this->serie_number_confirm) && !empty($this->model) && $this->model !== '--' && !empty($this->serie_number)) {

            if($this->serie_number_confirm !== $this->serie_number) {
                return new WP_Error('model', 'Les numéros de série ne correspondent pas.');
            }

            if(!in_array($this->model, $authorizedModels)) {
                return new WP_Error('model', 'Modèle inconnu.');
            }
        }


        if (strlen($this->password) < 5) {
            return new WP_Error('password', 'Le mot de passe doit comporter au moins 6 caractères.');
        }

        if ($this->password !== $this->password_confirm) {
            return new WP_Error('password', 'Les mots de passe ne correspondent pas.');
        }

        if (!is_email($this->email)) {
            return new WP_Error('email_invalid', 'Adresse email invalide.');
        }

        if (email_exists($this->email)) {
            return new WP_Error('email', 'Un compte existe déjà avec cette adresse email.');
        }

        $details = array('Username' => $this->email);

        foreach ($details as $field => $detail) {
            if (!validate_username($detail)) {
                return new WP_Error('name_invalid', 'Sorry, the "' . $field . '" you entered is not valid');
            }
        }

    }

    function registration()
    {
        global $wpdb;

        $resValidation = $this->validation();

        if (!is_wp_error($resValidation)) {

            $userdata = array(
                'user_login' => esc_attr($this->email),
                'user_email' => esc_attr($this->email),
                'user_pass' => esc_attr($this->password),
                'first_name' => esc_attr($this->first_name),
                'last_name' => esc_attr($this->last_name),
            );

            $register_user = wp_insert_user($userdata);
            if (!is_wp_error($register_user)) {

                update_user_meta( $register_user, 'wpcf-user-phone', esc_attr($this->phone));
                update_user_meta( $register_user, 'wpcf-user-city', esc_attr($this->city));
                update_user_meta( $register_user, 'wpcf-user-address', esc_attr($this->address));
                update_user_meta( $register_user, 'wpcf-user-zipcode', esc_attr($this->zipcode));

                if($this->optin == 'on') {
                    $this->optin = 1;
                } else {
                    $this->optin = 0;
                }

                update_user_meta( $register_user, 'wpcf-optin', esc_attr($this->optin));

                $to = $this->email;
                $subject = 'Confirmation de création de compte sur singerfrance.com';
                $body = '
                                <table>
                    <tr>
                        <td>
                            <img src="http://www.singerfrance.com/wp-content/themes/singer/assets/images/logo-mail-200.png" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Bonjour '.$this->first_name.' '.$this->last_name.',
                            <br /><br />
                            <p>
                                Votre inscription sur notre site <a href="http://www.singerfrance.com">singerfrance.com</a> a bien été prise en compte.
                            </p>
                            <p>
                                <ul>
                                    <li>Votre email: '.$this->email.'</li>
                                    <li>Votre mot de passe pour vous connecter: '.$this->password.'</li>
                                </ul>
                            </p>
                            <p>En cas de perte de votre mot de passe, utilisez ce lien : <a href="'.wp_lostpassword_url().'">mot de passe perdu.</a></p>
                            <p>A très bientôt sur <a href="http://www.singerfrance.com">singerfrance.com</a> !</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="margin-top:30px;">Suivez nos actualités et nos offres promotionnelles !</p>
                            <a href="https://www.facebook.com/SingerFR/"><img src="http://www.singerfrance.com/wp-content/themes/singer/assets/images/ico-fb.jpg" /></a> <a href="https://www.instagram.com/singerfrance/"><img src="http://www.singerfrance.com/wp-content/themes/singer/assets/images/icon-insta.jpg" /></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="margin:O;">---------------</p>
                            Cet e-mail vous est envoyé automatiquement du serveur <a href="http://www.singerfrance.com">singerfrance.com</a>, merci de ne pas y répondre.
                        </td>
                    </tr>
                </table>
                ';

                wp_mail( $to, $subject, $body);

                if(!empty($this->serie_number_confirm) && !empty($this->model) && $this->model !== '--' && !empty($this->serie_number)) {

                    if($this->serie_number_confirm == $this->serie_number) {
                        $wpdb->insert('singer_extension_garantie',array(
                            'id_user' => $register_user,
                            'model' => $this->model,
                            'serie_num' => $this->serie_number,
                            'lieu_achat' => $this->lieu_achat
                        ));

                        $to = $this->email;
                        $subject = 'Votre garantie supplémentaire Singer';
                        $body = '
                                <table>
                    <tr>
                        <td>
                            <img src="http://www.singerfrance.com/wp-content/themes/singer/assets/images/logo-mail-200.png" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Bonjour '.$this->first_name.' '.$this->last_name.',
                            <br /><br />
                            <p>
                                Nous vous remercions pour l\'enregistrement de votre numéro de série sur notre site <a href="http://www.singerfrance.com">singerfrance.com</a> Vous bénéficiez dès aujourd’hui d\'une garantie supplémentaire de 6 mois.
                            </p>
                            <p>Pour bénéficier de votre garantie, veuillez conserver votre facture d’achat.</p>
                            <p>
                                <ul>
                                    <li>Votre modèle de machine : '.$this->model.'</li>
                                    <li>Votre numéro de série : '.$this->serie_number.'</li>
                                </ul>
                            </p>
                            <p>A très bientôt sur <a href="http://www.singerfrance.com">singerfrance.com</a> !</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="margin-top:30px;">Suivez nos actualités et nos offres promotionnelles !</p>
                            <a href="https://www.facebook.com/SingerFR/"><img src="http://www.singerfrance.com/wp-content/themes/singer/assets/images/ico-fb.jpg" /></a> <a href="https://www.instagram.com/singerfrance/"><img src="http://www.singerfrance.com/wp-content/themes/singer/assets/images/icon-insta.jpg" /></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="margin:O;">---------------</p>
                            Cet e-mail vous est envoyé automatiquement du serveur <a href="http://www.singerfrance.com">singerfrance.com</a>, merci de ne pas y répondre.
                        </td>
                    </tr>
                </table>
                ';

                wp_mail( $to, $subject, $body);
                    }
                }

                wp_set_auth_cookie($register_user);
                wp_redirect(home_url('mon-compte'));
                die();
            } else {
                $this->userError = $register_user;
            }
        } else {
            $this->validation = $resValidation;
        }

    }

    function shortcode()
    {
        ob_start();
        $this->registration_form();
        return ob_get_clean();
    }

    function my_account_shortcode()
    {
        ob_start();
        $this->my_account();
        return ob_get_clean();
    }

    function extension_garantie_shortcode() {
        ob_start();
        $this->extension_garantie_form();
        return ob_get_clean();
    }

    function password_shortcode() {
        ob_start();
        $this->change_password_form();
        return ob_get_clean();
    }

    function edit_informations_shortcode() {
        ob_start();
        $this->edit_informations_form();
        return ob_get_clean();
    }

    function manage_register_form() {
        if ($_POST['reg_submit']) {
            $this->email = $_POST['reg_email'];
            $this->password = $_POST['reg_password'];
            $this->first_name = $_POST['reg_fname'];
            $this->last_name = $_POST['reg_lname'];
            $this->password_confirm = $_POST['reg_password_confirm'];
            $this->city = $_POST['reg_city'];
            $this->zipcode = $_POST['reg_zipcode'];
            $this->address = $_POST['reg_address'];
            $this->phone = $_POST['reg_phone'];
            $this->model = $_POST['reg_model'];
            $this->serie_number = $_POST['reg_serie_number'];
            $this->serie_number_confirm = $_POST['reg_serie_number_confirm'];
            $this->lieu_achat = $_POST['reg_lieu'];
            $this->optin = $_POST['optin'];

            $this->registration();
        }
    }

    function manage_extension_form() {
        if ($_POST['reg_submit_extension_garantie']) {
            $this->model = $_POST['reg_model'];
            $this->serie_number = $_POST['reg_serie_number'];
            $this->serie_number_confirm = $_POST['reg_serie_number_confirm'];
            $this->lieu_achat = $_POST['reg_lieu'];

            $this->extension();
        }
    }

    function manage_password_form() {
        if ($_POST['reg_submit_password']) {
            $this->password = $_POST['reg_password'];
            $this->password_confirm = $_POST['reg_password_confirm'];

            $this->password();
        }
    }

    function manage_edit_informations_form() {
        if ($_POST['reg_submit_informations']) {
            $this->email = $_POST['reg_email'];
            $this->first_name = $_POST['reg_fname'];
            $this->last_name = $_POST['reg_lname'];
            $this->city = $_POST['reg_city'];
            $this->zipcode = $_POST['reg_zipcode'];
            $this->address = $_POST['reg_address'];
            $this->phone = $_POST['reg_phone'];

            $this->editInformations();
        }
    }

    function editInformationsValidation() {
        if (empty($this->first_name) ||
            empty($this->last_name) ||
            empty($this->zipcode) ||
            empty($this->city) ||
            empty($this->address) ||
            empty($this->phone) ||
            empty($this->email)) {
            return new WP_Error('field', 'Des champs obligatoires ne sont pas renseignés');
        }

        if (!is_email($this->email)) {
            return new WP_Error('email_invalid', 'Adresse email invalide.');
        }

        $userData = get_userdata(get_current_user_id());

        if($userData->user_email !== $this->email) {
            if (email_exists($this->email)) {
                return new WP_Error('email', 'Un compte existe déjà avec cette adresse email.');
            }
        }
    }

    function editInformations() {
        global $wpdb;

        $resValidation = $this->editInformationsValidation();

        if (!is_wp_error($resValidation)) {

            $user_id = wp_update_user( array(
                'ID' => get_current_user_id(),
                'user_email' => $this->email,
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
            ) );

            if ( is_wp_error( $user_id ) ) {
                $this->validation = new WP_Error('model', 'Une erreur est survenue lors de la mise à jour des informations.');
            } else {
                update_user_meta( $user_id, 'wpcf-user-phone', esc_attr($this->phone));
                update_user_meta( $user_id, 'wpcf-user-city', esc_attr($this->city));
                update_user_meta( $user_id, 'wpcf-user-address', esc_attr($this->address));
                update_user_meta( $user_id, 'wpcf-user-zipcode', esc_attr($this->zipcode));

                wp_redirect(home_url('mon-compte').'?message_type=3');
                die();
            }

        } else {
            $this->validation = $resValidation;
        }
    }

    function passwordValidation() {
        $models = get_posts( array('post_type' => 'produit', 'posts_per_page' => -1));

        foreach ($models as $model) {
            $authorizedModels[] = $model->post_title;
        }


        if(empty($this->password) || empty($this->password_confirm)) {
            return new WP_Error('model', 'Les champs sont incomplets.');
        }

        if($this->password !== $this->password_confirm) {
            return new WP_Error('model', 'Les mots de passe ne correspondent pas.');
        }
    }

    function password() {
        global $wpdb;

        $resValidation = $this->passwordValidation();

        if (!is_wp_error($resValidation)) {

            $user_id = wp_update_user( array( 'ID' => get_current_user_id(), 'user_pass' => $this->password ) );

            if ( is_wp_error( $user_id ) ) {
                $this->validation = new WP_Error('model', 'Une erreur est survenue lors de la mise à jour du mot de passe.');
            } else {
                wp_redirect(home_url('mon-compte').'?message_type=2');
                die();
            }

        } else {
            $this->validation = $resValidation;
        }
    }

    function extensionValidation() {
        $models = get_posts( array('post_type' => 'produit', 'posts_per_page' => -1));

        foreach ($models as $model) {
            $authorizedModels[] = $model->post_title;
        }


        if(($this->model == '--') || (empty($this->serie_number) || empty($this->serie_number_confirm)) || $this->lieu_achat == '--') {
            return new WP_Error('model', 'Les champs pour obtenir l\'extension de garantie sont incomplets.');
        }

        if($this->serie_number_confirm !== $this->serie_number) {
            return new WP_Error('model', 'Les numéros de série ne correspondent pas.');
        }

        if(!in_array($this->model, $authorizedModels)) {
            return new WP_Error('model', 'Modèle inconnu.');
        }

    }

    function extension() {
        global $wpdb;

        $resValidation = $this->extensionValidation();

        if (!is_wp_error($resValidation)) {
            $wpdb->insert('singer_extension_garantie',array(
                'id_user' => get_current_user_id(),
                'model' => $this->model,
                'serie_num' => $this->serie_number,
                'lieu_achat' => $this->lieu_achat
            ));
            $currentUser = wp_get_current_user();
            $userData = get_userdata($currentUser->ID);

            $to = $userData->data->user_email;
            $subject = 'Votre garantie supplémentaire Singer';
            $body = '
                                <table>
                    <tr>
                        <td>
                            <img src="http://www.singerfrance.com/wp-content/themes/singer/assets/images/logo-mail-200.png" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Bonjour '.get_user_meta($currentUser->ID, 'first_name', true).' '.get_user_meta($currentUser->ID, 'last_name', true).',
                            <br /><br />
                            <p>
                                Nous vous remercions pour l\'enregistrement de votre numéro de série sur notre site <a href="http://www.singerfrance.com">singerfrance.com</a> Vous bénéficiez dès aujourd’hui d\'une garantie supplémentaire de 6 mois.
                            </p>
                            <p>Pour bénéficier de votre garantie, veuillez conserver votre facture d’achat.</p>
                            <p>
                                <ul>
                                    <li>Votre modèle de machine : '.$this->model.'</li>
                                    <li>Votre numéro de série : '.$this->serie_number.'</li>
                                </ul>
                            </p>
                            <p>A très bientôt sur <a href="http://www.singerfrance.com">singerfrance.com</a> !</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="margin-top:30px;">Suivez nos actualités et nos offres promotionnelles !</p>
                            <a href="https://www.facebook.com/SingerFR/"><img src="http://www.singerfrance.com/wp-content/themes/singer/assets/images/ico-fb.jpg" /></a> <a href="https://www.instagram.com/singerfrance/"><img src="http://www.singerfrance.com/wp-content/themes/singer/assets/images/icon-insta.jpg" /></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="margin:O;">---------------</p>
                            Cet e-mail vous est envoyé automatiquement du serveur <a href="http://www.singerfrance.com">singerfrance.com</a>, merci de ne pas y répondre.
                        </td>
                    </tr>
                </table>
                ';

            wp_mail( $to, $subject, $body);
            wp_redirect(home_url('mon-compte').'?message_type=1');
            die();

        } else {
            $this->validation = $resValidation;
        }
    }

    function my_account() {
        global $wpdb;

        $currentUser = wp_get_current_user();
        $userData = get_userdata($currentUser->ID);

        $results = $wpdb->get_results( 'SELECT * FROM singer_extension_garantie WHERE id_user = '.$currentUser->ID, OBJECT );

        if(isset($_GET['message_type']) && $_GET['message_type'] == '1') {
            $message = 'Votre extension de garantie est prise en compte !';
        }

        if(isset($_GET['message_type']) && $_GET['message_type'] == '2') {
            $message = 'Votre mot de passe a été changé !';
        }

        if(isset($_GET['message_type']) && $_GET['message_type'] == '3') {
            $message = 'Vos informations sont désormais à jour !';
        }

        if(isset($message)) {
            echo '<div class="row"><div class="col-lg-6 col-lg-push-3"><div class="success-account">';
            echo '<strong>' . $message . '</strong>';
            echo '</div></div></div>';
        }

        ?>

        <div class="row account">
            <div class="col-lg-6 col-lg-push-3">
                <div class="row">
                    <div class="col-md-6 text-center">
                        <a href="<?php echo esc_url(home_url('mon-compte/modifier-mes-informations')); ?>">Modifier mon compte</a>
                    </div>
                    <div class="col-md-6 text-center">
                        <a href="<?php echo esc_url(home_url('mon-compte/changer-mon-mot-de-passe')); ?>">Changer mon mot de passe</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h2>Informations de compte</h2>
                        <div class="block">
                            <div class="row">
                                <div class="col-md-4">
                                    Adresse E-mail :
                                </div>
                                <div class="col-md-8">
                                    <?php echo $userData->data->user_email; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h2>Mes données personnelles</h2>
                        <div class="block">
                            <div class="row">
                                <div class="col-md-4">
                                    Prénom
                                </div>
                                <div class="col-md-8">
                                    <?php echo get_user_meta($currentUser->ID, 'first_name', true) ?>
                                </div>
                                <div class="col-md-4">
                                    Nom
                                </div>
                                <div class="col-md-8">
                                    <?php echo get_user_meta($currentUser->ID, 'last_name', true) ?>
                                </div>
                                <div class="col-md-4">
                                    Adresse
                                </div>
                                <div class="col-md-8">
                                    <?php echo get_user_meta($currentUser->ID, 'wpcf-user-address', true) ?>
                                    <br />
                                    <?php echo get_user_meta($currentUser->ID, 'wpcf-user-zipcode', true) ?> <?php echo get_user_meta($currentUser->ID, 'wpcf-user-city', true) ?>
                                </div>
                                <div class="col-md-4">
                                    Téléphone
                                </div>
                                <div class="col-md-8">
                                    <?php echo get_user_meta($currentUser->ID, 'wpcf-user-phone', true) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h2>Extension de garantie singer</h2>
                        <div class="block text-center">
                            <?php if(count($results) > 0): ?>
                                <ul class="garantie-list">
                                    <?php foreach ($results as $result): ?>
                                        <li>Extension de garantie pour le modèle <strong><?php echo $result->model; ?></strong> (<?php echo $result->serie_num; ?>)</li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                            <a href="<?php echo esc_url(home_url('mon-compte/extension-de-garantie/')); ?>" class="btn btn-primary">Enregistrer une autre machine</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}

new Singer_registration_form;
