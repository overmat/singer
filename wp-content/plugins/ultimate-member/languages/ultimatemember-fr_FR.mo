��          �   %   �      P     Q     `  
   p     {     �     �     �     �  
   �     �     �          3     N  #   i     �     �     �     �     �  I   �  O     "   _     �     �  �  �     �     �     �  )   �     �     �          !     0  !   ?  %   a     �  "   �  )   �  6   �     (	     5	     B	     N	     T	  B   t	  H   �	  '    
  )   (
  $   R
                                   
                              	                                                            %s is required %s is required. Confirm %s Enter your username or email Field Required Asterisk Color Forgot your password? Keep me signed in Login Login page Password Reset Please confirm your password Please enter your email Please enter your password Please enter your username Please enter your username or email Register Register page Required Reset Reset my password To reset your password, please enter your email address or username below We have sent you a password reset link to your e-mail. Please check your inbox. You must confirm your new password You must enter a new password You must enter your password Project-Id-Version: Ultimate Member
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-01-07 21:38+0800
PO-Revision-Date: 2017-05-09 01:50+0200
Last-Translator: Calum Allison <umplugin@gmail.com>
Language-Team: Ultimate Member <umplugin@gmail.com>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: .git
X-Poedit-SearchPathExcluded-1: node_modules
X-Poedit-SearchPathExcluded-2: admin/core/lib/ReduxFramework
X-Poedit-SearchPathExcluded-3: assets/vendor
 %s est obligatoire %s est obligatoire. Confirmation %s Entrer votre email ou nom d’utilisateur *Champs obligatoire Mot de passe oublié ? Se souvenir de moi Je me connecte Je me connecte Réinitialisation du mot de passe Veuillez confirmez votre mot de passe Veuillez entrer votre email. Veuillez entrer votre mot de passe Veuillez entrer votre nom d’utilisateur Entrer votre adresse mail ou votre nom d’utilisateur S’inscrire S’inscrire Obligatoire Reset Réinitialiser mon mot de passe Veuillez entrer votre email pour réinitialiser votre mot de passe Nous vous avons envoyé un email afin de créer un nouveau mot de passe. Vous devez confirmer votre mot de passe Vous devez entrer un nouveau mot de passe Vous devez entrer votre mot de passe 