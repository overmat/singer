/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
          $('#carousel').flexslider({
              animation: "slide",
              controlNav: false,
              animationLoop: false,
              slideshow: false,
              itemWidth: 210,
              itemMargin: 5,
              asNavFor: '#slider',
              prevText: '<',
              nextText: '>'
          });

          $('#slider').flexslider({
              animation: "slide",
              controlNav: false,
              animationLoop: false,
              slideshow: false,
              directionNav: false,
              minItems: 1,
              maxItems: 5,
              sync: "#carousel",
              start: function(slider){
                  $('body').removeClass('loading');
              }
          });

          $(document).on('click', '.page-id-229 .um-button', function() {
                if($('input[name="serial_number-209"]').val() != $('input[name="serie_conf-209"]').val()) {
                    $('input[name="serie_conf-209"]').parent().append('<p class="error">Merci de confirmer votre numéro de série.</p>');
                    return false;
                }
          });

          if($('#lieu-achat').val() == 'Magasins spécialistes') {
              $('#city-buy-209').parent().parent().show();
          } else {
              $('#city-buy-209').parent().parent().hide();
          }

          $(document).on('change', '#lieu-achat', function() {
              if($('#lieu-achat').val() == 'Magasins spécialistes') {
                  console.log('looool');
                  $('#city-buy-209').parent().parent().show();
              } else {
                  $('#city-buy-209').parent().parent().hide();
              }
          });

          jQuery(document).ready(function($){
              $('#timeline').timelinr({
                  autoPlay: 'false',
                  autoPlayDirection: 'forward',
                  startAt: 2,
                  issuesTransparency: 0
              });

              $("#light-slider").lightSlider({
                  gallery: true,
                  item: 1,
                  loop:true,
                  slideMargin: 5,
                  thumbItem: 3,
                  galleryMargin: 5
              });
          });

          $('#slider-menu').slideReveal({
              trigger: $("#nav-icon3"),
              push: false,
              width: '100%'
          });

          $(document).on('click', '#about-more', function(e) {
              e.preventDefault();
              $('#about-text').slideToggle('slow');
              return false;

          });

          $("#wp_pass_reset").submit(function() {
              $('#result').html('<span class="loading">Veuillez patienter...</span>').fadeIn();
              var url = $(this).attr('action');
              var input_data = $('#wp_pass_reset').serialize();
              $('#submitbtn').prop("disabled",true);
              $.ajax({
                  type: "POST",
                  url:  url,
                  data: input_data,
                  success: function(msg){
                      $('.loading').remove();
                      $('<div>').html(msg).appendTo('div#result').hide().fadeIn('slow');
                      $('#user_input').val('');
                      $('#submitbtn').prop("disabled",false);
                  }
              });
              return false;

          });

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired

          $(document).ready(function(){
              $('#nav-icon3').click(function(){
                  $(this).toggleClass('open');
              });
          });
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

  $(document).ready(function() {
      $('form[autocomplete="off"] input, input[autocomplete="off"]').each(function () {

          var input = this;
          var name = $(input).attr('name');
          var id = $(input).attr('id');

          $(input).removeAttr('name');
          $(input).removeAttr('id');

          setTimeout(function () {
              $(input).attr('name', name);
              $(input).attr('id', id);
          }, 1);
      });
  })

})(jQuery); // Fully reference jQuery after this point.
