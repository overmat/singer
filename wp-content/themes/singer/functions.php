<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

register_nav_menus( array(
    'Footer_1' => 'Menu footer 1',
    'Footer_2' => 'Menu footer 2',
    'Main' => 'Navigation principale'
) );


// Replaces the excerpt "Read More" text by a link
function new_excerpt_more($more) {
    global $post;
    return '<span class="moretag"> Lire la suite</span>';
}
add_filter('excerpt_more', 'new_excerpt_more');

add_filter('sage/display_sidebar', 'sage_sidebar_on_special_page');

function sage_sidebar_on_special_page($sidebar) {

    if (is_page('communication')) {
        return true;
    }

    return $sidebar;
}

function fjarrett_get_attachment_id_by_url( $url ) {

    // Split the $url into two parts with the wp-content directory as the separator
    $parsed_url  = explode( parse_url( WP_CONTENT_URL, PHP_URL_PATH ), $url );

    // Get the host of the current site and the host of the $url, ignoring www
    $this_host = str_ireplace( 'www.', '', parse_url( home_url(), PHP_URL_HOST ) );
    $file_host = str_ireplace( 'www.', '', parse_url( $url, PHP_URL_HOST ) );

    // Return nothing if there aren't any $url parts or if the current host and $url host do not match
    if ( ! isset( $parsed_url[1] ) || empty( $parsed_url[1] ) || ( $this_host != $file_host ) ) {
        return;
    }

    // Now we're going to quickly search the DB for any attachment GUID with a partial path match

    // Example: /uploads/2013/05/test-image.jpg
    global $wpdb;
    $attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}posts WHERE guid RLIKE %s;", $parsed_url[1] ) );

    // Returns null if no attachment is found
    return $attachment[0];
}

add_image_size( 'gamme-vignette', 1110, 330, true );


function singer_numeric_posts_nav() {

    if( is_singular() )
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    /**	Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;

    /**	Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="navigation"><ul class="clearfix">' . "\n";

    /**	Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

    /**	Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }

    /**	Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    /**	Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }

    /**	Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li>%s</li>' . "\n", get_next_posts_link() );

    echo '</ul></div>' . "\n";
}

function add_query_vars_filter( $vars ){
    $vars[] = "showdate";
    return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );

add_filter( 'wpsl_store_meta', 'custom_store_meta', 10, 2 );

function custom_store_meta( $store_meta, $store_id ) {

    $terms = wp_get_post_terms( $store_id, 'wpsl_store_category' );

    $store_meta['terms'] = '';

    if ( $terms ) {
        if ( !is_wp_error( $terms ) ) {
            if ( count( $terms ) > 1 ) {
                $location_terms = array();

                foreach ( $terms as $term ) {
                    $location_terms[] = $term->name;
                }

                $store_meta['terms'] = implode( ', ', $location_terms );
            } else {
                $store_meta['terms'] = $terms[0]->name;
            }
        }
    }

    return $store_meta;
}

add_action('init', 'register_rewrites');
function register_rewrites() {
    add_rewrite_rule('^nos-produitsl$', 'index.php?post_type=produit','top');
}

add_image_size( 'actu-thumbnail', 301, 134, array( 'center', 'center' ) );


function timeline_tag( $atts ) {

    $posts = query_posts( array('post_type' => 'element-chrono'));

    $res = '<div id="timeline">
        <div class="date-wrapper clearfix">
            <ul id="dates">';
                foreach ($posts as $post):
                    $res .= '<li><a href="#' .  get_field('wpcf-date-chrono', $post->ID) . '">'.  get_field('wpcf-date-chrono', $post->ID) .'<div class="layer"></div></a></li>';
                endforeach;
            $res .= '</ul>
        </div>
        <ul id="issues">';
            foreach ($posts as $post):
                $res .= '<li id="'.  get_field('wpcf-date-chrono', $post->ID) . '">
                    <img src="'. get_the_post_thumbnail_url($post) .'"/>
                    <div class="content-chrono">
                        '.  $post->post_content .'
                    </div>
                </li>';
            endforeach;
        $res .= '</ul>
        <a href="#" id="next">
            <img src="'.get_template_directory_uri().'/dist/images/chrono-left.png" />
        </a>
        <a href="#" id="prev">
            <img src="'.get_template_directory_uri().'/dist/images/chrono-right.png" />
        </a>
    </div>';

    return $res;
}

add_filter( 'init', 'my_registration_page_redirect' );

function my_registration_page_redirect()
{
    global $pagenow;

    if ( ( strtolower($pagenow) == 'wp-login.php') && ( strtolower( $_GET['action']) == 'register' ) ) {
        wp_redirect( home_url('inscription'));
    }
}

add_shortcode( 'chrono', 'timeline_tag' );

add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

        $title = single_cat_title( '', false );

    } elseif ( is_tag() ) {

        $title = single_tag_title( '', false );

    } elseif ( is_author() ) {

        $title = '<span class="vcard">' . get_the_author() . '</span>' ;

    }

    return $title;

});

// Function to change email address

function wpb_sender_email( $original_email_address ) {
    return 'noreply@singerfrance.com';
}

// Function to change sender name
function wpb_sender_name( $original_email_from ) {
    return 'Singer France';
}

// Hooking up our functions to WordPress filters
add_filter( 'wp_mail_from', 'wpb_sender_email' );
add_filter( 'wp_mail_from_name', 'wpb_sender_name' );

function wpse27856_set_content_type(){
    return "text/html";
}
add_filter( 'wp_mail_content_type','wpse27856_set_content_type' );


function mysite_show_extra_profile_fields($user) {
    global $wpdb;
    $extensions = $wpdb->get_results('SELECT * FROM singer_extension_garantie WHERE id_user = '.$user->id);
    $models = get_posts( array('post_type' => 'produit', 'posts_per_page' => -1));
    print('<h3>Extensions de garantie</h3>');

    echo '<table class="wp-list-table widefat fixed striped users"><thead><tr><th>Modèle</th><th>Numéro de série</th><th>Lieu d\'achat</th><th>Action</th></tr></thead><tbody>';

    foreach ($extensions as $ex) {
        echo '<tr><td>'.$ex->model.'</td><td>'.$ex->serie_num.'</td><td>'.$ex->lieu_achat.'</td><td><a href="#" class="remove-num" data-id="'.$ex->id.'">Supprimer</a></td>';
    }

    if(empty($extensions)) {
        echo '<tr><td align="center" colspan="3">Aucune extensions de garantie</td>';
    }
    echo '</tbody></table>';
    echo '<h4>Ajouter un numéro de série</h4>';
    echo '<select name="model" id="model"><option value="--">Sélectionnez le modèle</option>';
        foreach ($models as $model):
            $terms = wp_get_post_terms($model->ID, 'catalogue');
            $metas = get_all_wp_terms_meta($terms[0]->term_id);
            if($metas['garantie'][0] == 'checked'):
                echo '<option value="'.$model->post_title.'"">'.$model->post_title.'</option>';
            endif;
        endforeach;
    echo '</select>';

    echo '<select name="lieu_achat" class="form-control" id="lieu_achat">
                            <option value="--">Selectionnez le lieu d\'achat</option>
                            <option value="Sur internet">Sur internet</option>
                            <option value="Grande distribution">Grande distribution</option>
                            <option value="Magasin Singer">Magasin Singer</option>
                        </select> Numéro de série : <input type="text" name="serie_num" id="serie_num"> <input type="button" id="sbt_garantie" name="sbt_garantie" value="Ajouter" class="button button-primary"/>';
    echo '<input type="hidden" value="'.$user->id.'" id="g_user_id" name="g_user_id" />';
    echo "
    <script>
        jQuery(document).on('click', '#sbt_garantie', function() {
            jQuery(this).prop(\"disabled\",true);
            if(jQuery('#serie_num').val() == '' || jQuery('#model').val() == '--' || jQuery('#lieu_achat').val() == '--') {
                alert('Merci de renseigner tous les champs');
            } else { 
            jQuery.post(
                ajaxurl,
                {
                    'action': 'mon_action',
                    'serie_num': jQuery('#serie_num').val(),
                    'model': jQuery('#model').val(),
                    'lieu_achat': jQuery('#lieu_achat').val(),
                    'user_id': jQuery('#g_user_id').val()
                },
                function(response){
                     location.reload();
                   }
                );
            }
            jQuery(this).prop(\"disabled\",false);
        });
        jQuery(document).on('click', '.remove-num', function() {
            var id = jQuery(this).attr('data-id');
            jQuery.post(
                ajaxurl,
                {
                    'action': 'mon_action_remove',
                    'id': id,
                },
                function(response){
                     location.reload();
                   }
             );
             
             return false;
        });
    </script>
    ";


}

add_action('show_user_profile', 'mysite_show_extra_profile_fields');
add_action('edit_user_profile', 'mysite_show_extra_profile_fields');

function tg_validate_url() {
    $page_url = esc_url(site_url('/mot-de-passe-oublie/'));

    $urlget = strpos($page_url, "?");
    if ($urlget === false) {
        $concate = "?";
    } else {
        $concate = "&";
    }
    return $page_url.$concate;
}

add_filter( 'lostpassword_url',  'wdm_lostpassword_url', 10, 0 );
function wdm_lostpassword_url() {
    return site_url('/mot-de-passe-oublie/');
}

add_action( 'wp_ajax_mon_action', 'mon_action' );
add_action( 'wp_ajax_nopriv_mon_action', 'mon_action' );

function mon_action() {
    global $wpdb;
    $wpdb->insert('singer_extension_garantie',array(
        'id_user' => $_POST['user_id'],
        'model' => $_POST['model'],
        'serie_num' => $_POST['serie_num'],
        'lieu_achat' => $_POST['lieu_achat']
    ));
}

add_action( 'wp_ajax_mon_action_remove', 'mon_action_remove' );
add_action( 'wp_ajax_nopriv_mon_action_remove', 'mon_action_remove' );

function mon_action_remove() {
    global $wpdb;
    if(is_numeric($_POST['id'])) {
        $id = $_POST['id'];
        $wpdb->delete('singer_extension_garantie', array( 'id' => $id ) );
    }
}

// remove notifications in CSS
add_action('admin_head', 'remove_update_on_css');

function remove_update_on_css() {
    echo '<style>.update-count,.plugin-count,.update-nag,#wp-admin-bar-updates{display:none!important}</style>';
}

remove_action('load-update-core.php', 'wp_update_themes' );
add_filter( 'pre_site_transient_update_themes', create_function('$a', "return null;"));

// remove notifications core
add_filter('pre_site_transient_update_core', create_function('$a', "return null;"));

// remove notifications plugins
remove_action('load-update-core.php', 'wp_update_plugins');
add_filter('pre_site_transient_update_plugins', create_function('$a', "return null;"));

add_action( 'admin_init', 'wpse_38111' );
function wpse_38111() {
    remove_submenu_page( 'index.php', 'update-core.php' );
}

/**
 * Enqueue the date picker
 */
function enqueue_date_picker(){
                wp_enqueue_script(
            'field-date-js', 
            get_home_url().'/wp-content/themes/singer/admin/js/field_date.js', 
            array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker'),
            time(),
            true
        );  

        wp_enqueue_style( 'jquery-ui-datepicker' );
}

add_action( 'admin_enqueue_scripts', 'enqueue_date_picker' );
