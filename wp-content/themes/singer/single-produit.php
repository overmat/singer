<?php
    $isMachine = get_field('wpcf-page-machine');

    if($isMachine == '1') {
        include( locate_template( 'templates/product-machine.php' ) );
    } else {
        include( locate_template( 'templates/product-classic.php' ) );
    }
?>