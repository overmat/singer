<?php
    $customMetas = get_post_custom($post->ID);
    $opening = unserialize($customMetas['wpsl_hours'][0]);
    $daysTrad = array('monday' => 'Lundi', 'tuesday' => 'Mardi', 'wednesday' => 'Mercredi', 'thursday' => 'Jeudi', 'friday' => 'Vendredi', 'saturday' => 'Samedi', 'sunday' => 'Dimanche');
    $terms = wp_get_post_terms( $post->ID, 'wpsl_store_category' );

?>
<div class="row">
    <div class="col-md-12">
        <div class="fil">
            <?php
            if(function_exists('bcn_display'))
            {
                bcn_display();
            }
            ?>
        </div>
    </div>
</div>
<div class="row single-store">
    <div class="col-md-4">
        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="store-img img-responsive"/>
        <div class="store-content">
            <?php
                $content = get_post()->post_content;
                $content = apply_filters( 'the_content', $content );
                $content = str_replace( ']]>', ']]&gt;', $content );
                echo $content;
            ?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="wraper-detail">
            <h1><?php the_title(); ?></h1>
            <?php if(isset($terms[0])): ?>
                <div class="cat-name"><?php echo $terms[0]->name; ?></div>
            <?php endif; ?>
            <div class="block-address">
                <p><?php echo $customMetas['wpsl_address'][0]; ?></p>
                <p><?php echo $customMetas['wpsl_zip'][0]; ?> <?php echo $customMetas['wpsl_city'][0]; ?></p>
            </div>
            <?php if($opening !== null && $opening !== '' && $opening !== false): ?>
            <div class="hours-opening">
                <?php foreach($opening as $day => $hour):?>
                <p class="hours">
                    <span class="label"><?php echo $daysTrad[$day]; ?></span><span class="value"><?php echo ($hour[0] == '') ? 'Fermé':str_replace(',',' à ',$hour[0]); ?> <?php echo ($hour[1] == '') ? '':' - '. str_replace(',',' à ',$hour[1]); ?></span>
                </p>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>
            <div class="more-info">
                <?php if (isset($customMetas['wpsl_phone'])): ?>
                <p>
                    <span class="label">Numéro</span><span class="value"><?php echo $customMetas['wpsl_phone'][0]; ?></span>
                </p>
                <?php endif; ?>

                <?php if (isset($customMetas['wpsl_email'])): ?>
                    <p>
                        <span class="label">Email</span><span class="value"><a href="mailto:<?php echo $customMetas['wpsl_email'][0]; ?>"><?php echo $customMetas['wpsl_email'][0]; ?></a></span>
                    </p>
                <?php endif; ?>
                <?php if (isset($customMetas['wpsl_url'])): ?>
                    <p>
                        <span class="label">Site</span><span class="value"><a href="<?php echo $customMetas['wpsl_url'][0]; ?>"><?php echo $customMetas['wpsl_url'][0]; ?></a></span>
                    </p>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php
            echo do_shortcode( '[wpsl_map zoom="14" height="350"]' );
            $queried_object = get_queried_object();
            $address       = get_post_meta( $queried_object->ID, 'wpsl_address', true );
            $city          = get_post_meta( $queried_object->ID, 'wpsl_city', true );
            $country       = get_post_meta( $queried_object->ID, 'wpsl_country', true );
            $destination   = $address . ',' . $city . ',' . $country;
            $direction_url = "https://maps.google.com/maps?saddr=&daddr=" . urlencode( $destination ) . "";

            echo '<p><a target="_blank" href="' . esc_url( $direction_url ) . '">S\'y rendre</a></p>';
        ?>

    </div>
</div>