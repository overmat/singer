<?php
    $termMain = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    $metasMain = get_all_wp_terms_meta($termMain->term_id);


    $children = get_term_children( $termMain->term_id, $termMain->taxonomy );

    $children = get_terms( $termMain->taxonomy, array( 'child_of' => $termMain->term_id ) );

    $empty = true;
    if(count($children) > 0) {
        $empty = false;
        foreach ($children as $id_term) {
            $term = get_term($id_term);
            include( locate_template( 'templates/term-child.php' ) );
        }
    }


    if($empty) {
        include( locate_template( 'templates/term-list-product.php' ) );
    }

?>

<?php if($empty): ?>
    <div class="alert alert-warning">
        <?php _e('Aucun produit pour le moment...', 'sage'); ?>
    </div>
<?php endif; ?>