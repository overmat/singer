<?php
$termMain = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

query_posts( array('post_type' => array('tuto', 'video'), 'posts_per_page' => -1, 'tax_query' => array(
    array(
        'taxonomy' => 'categories-tuto',
        'field' => 'term_id',
        'terms' => $termMain->term_id,
    )

)) );

?>
    <div class="row">
        <div class="col-md-12">
            <div class="fil">
                <!-- Breadcrumb NavXT 5.7.0 -->
                <span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to Singer France." href="/" class="home"><span property="name">Singer France</span></a><meta property="position" content="1"></span> &gt; <span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to Vidéos et tutos." href="https://www.singerfrance.com/videos-et-tutos/" class="post post-tuto-archive"><span property="name">Vidéos et tutos</span></a><meta property="position" content="2"></span> &gt; <span property="itemListElement" typeof="ListItem"><span property="name"><?php echo $termMain->name; ?></span><meta property="position" content="3"></span>        </div>
        </div>
    </div>
    <div class="page-header">
        <h1><?php echo $termMain->name; ?></h1>
    </div>

<?php if (!have_posts()) : ?>
    <div class="alert alert-warning">
        <?php _e('Aucun article pour le moment...', 'sage'); ?>
    </div>
<?php endif; ?>

    <div class="row">
        <?php while (have_posts()) : the_post(); ?>
            <div class="col-md-4">
                <?php if(get_post()->post_type == 'video'): ?>
                <a class="link-to-post" data-fancybox href="<?php echo get_field('wpcf-video'); ?>">
                    <?php else: ?>
                    <a class="link-to-post" href="<?php the_permalink(); ?>">
                        <?php endif; ?>
                        <article <?php post_class(); ?>>
                            <div class="row">
                                <div class="col-md-6">
                                    <figure class="post-thumbnail post-<?php echo get_post()->post_type; ?>">

                                        <?php the_post_thumbnail(); ?>
                                        <?php if(get_post()->post_type == 'video'): ?>
                                            <div class="layer-video"></div>
                                        <?php endif; ?>
                                    </figure>
                                </div>
                                <div class="col-md-6 col-right">
                                    <header>
                                        <h1 class="entry-title"><?php the_title(); ?></h1>
                                    </header>
                                    <?php if(get_post()->post_type != 'video'): ?>
                                        <div class="entry-content">
                                            <p class="level">Niveau :
                                                <?php for($i=0;$i< (int)get_field('wpcf-niveau');$i++): ?>
                                                    <img class="img-star" src="<?php echo get_template_directory_uri(); ?>/assets/images/star-black.jpg" />
                                                <?php endfor; ?>
                                                <?php for($i=0;$i< 3 -(int)get_field('wpcf-niveau');$i++): ?>
                                                    <img class="img-star" src="<?php echo get_template_directory_uri(); ?>/assets/images/star-white.jpg" />
                                                <?php endfor; ?>
                                            </p>
                                            <p class="duration">
                                                Durée : <?php echo get_field('wpcf-duree'); ?>
                                            </p>
                                            <p class="subtitle"><?php echo get_field('wpcf-sous-titre'); ?></p>
                                        </div>
                                    <?php else: ?>
                                        <div class="entry-content">
                                            <p>
                                                <?php echo get_field('wpcf-description-courte'); ?>
                                            </p>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </article>
                    </a>
            </div>
        <?php endwhile; ?>
    </div>
<?php wp_reset_postdata(); ?>