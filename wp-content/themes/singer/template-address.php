<?php
/**
 * Template Name: Gabarit modifier adresse
 */
?>

<div class="page-header">
    <h1><?php echo get_the_title(); ?></h1>
</div>
<div class="um um-account um-editing um-um_account_id">
    <div class="row">
        <div class="col-md-6 col-md-push-3">
            <div class="um-form">
                <form method="post" action="">
                    <div class="um-row _um_row_1 ">
                        <div class="um-col-1">
                            <div class="um-field um-field-user_adress um-field-text" data-key="user_adress">
                                <div class="um-field-label">
                                    <label for="user_adress-245">Adresse</label>
                                    <div class="um-clear"></div>
                                </div>
                                <div class="um-field-area">
                                    <input autocomplete="off" class="um-form-field um-error " type="text" name="user_adress-245" id="user_adress-245" value="" placeholder="" data-validate="" data-key="user_adress">
                                </div>
                                <div class="um-field-error">
                                    <span class="um-field-arrow">
                                        <i class="um-faicon-caret-up"></i>
                                    </span>
                                    Adresse est obligatoire
                                </div>
                            </div>
                            <div class="um-field um-field-zip_code um-field-text" data-key="zip_code">
                                <div class="um-field-label">
                                    <label for="zip_code-245">Code postal</label>
                                    <div class="um-clear"></div>
                                </div>
                                <div class="um-field-area">
                                    <input autocomplete="off" class="um-form-field um-error " type="text" name="zip_code-245" id="zip_code-245" value="" placeholder="" data-validate="" data-key="zip_code">
                                </div>
                                <div class="um-field-error">
                                    <span class="um-field-arrow">
                                        <i class="um-faicon-caret-up"></i>
                                    </span>
                                    Code postal est obligatoire
                                </div>
                            </div>
                            <div class="um-field um-field-city um-field-text" data-key="city">
                                <div class="um-field-label">
                                    <label for="city-245">Ville</label>
                                    <div class="um-clear"></div>
                                </div>
                                <div class="um-field-area">
                                    <input autocomplete="off" class="um-form-field um-error " type="text" name="city-245" id="city-245" value="" placeholder="" data-validate="" data-key="city">
                                </div>
                                <div class="um-field-error">
                                    <span class="um-field-arrow">
                                        <i class="um-faicon-caret-up"></i>
                                    </span>
                                    Ville est obligatoire
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="um-col-alt">
                        <div class="um-center"><input type="submit" value="S'inscrire" class="um-button"></div>
                        <div class="um-clear"></div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

