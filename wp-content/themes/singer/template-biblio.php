<?php
/**
 * Template Name: Gabarit bibliothèque
 */
?>
<?php
query_posts( array(
    'post_type' => 'livre',
    'paged' => $paged,
    'posts_per_page' => -1
) );
?>
<div class="row">
    <div class="col-md-12">
        <div class="fil">
            <?php
            if(function_exists('bcn_display'))
            {
                bcn_display();
            }
            ?>
        </div>
    </div>
</div>
<?php if (!have_posts()) : ?>
    <div class="alert alert-warning">
        <?php _e('Aucun article pour le moment...', 'sage'); ?>
    </div>
<?php endif; ?>

<div class="page-header">
    <h1><?php echo get_the_title(); ?></h1>
</div>
<div class="row">
    <?php while (have_posts()) : the_post(); ?>
        <div class="col-md-6">
                <article <?php post_class(); ?>>
                    <div class="row">
                        <div class="col-md-4">
                            <figure class="post-thumbnail">
                                <?php the_post_thumbnail(); ?>
                            </figure>
                        </div>
                        <div class="col-md-8">
                            <header>
                                <h1 class="entry-title"><?php the_title(); ?></h1>
                                <p class="author">Ecrit par <?php echo get_field('wpcf-auteur'); ?></p>
                            </header>
                            <div class="entry-content">
                                <?php the_content(); ?>
                            </div>
                            <p class="where"><?php echo get_field('wpcf-lieu'); ?></p>
                        </div>
                    </div>
                </article>
        </div>
    <?php endwhile; ?>
</div>

<?php wp_reset_postdata(); ?>

