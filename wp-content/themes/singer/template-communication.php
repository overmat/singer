<?php
/**
 * Template Name: Gabarit communication
 */
?>
<?php
    query_posts( array('category_name' => 'communication') );
?>
<div class="row">
    <div class="col-md-12">
        <div class="fil">
            <?php
            if(function_exists('bcn_display'))
            {
                bcn_display();
            }
            ?>
        </div>
        <div class="page-header">
            <h1>Actualités</h1>
        </div>
    </div>
</div>
<?php if (!have_posts()) : ?>
    <div class="alert alert-warning">
        <?php _e('Aucun article pour le moment...', 'sage'); ?>
    </div>
<?php endif; ?>
<div class="row">
<?php while (have_posts()) : the_post(); ?>
    <div class="col-md-4">
        <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
    </div>
<?php endwhile; ?>
</div>

<?php singer_numeric_posts_nav(); ?>

<?php wp_reset_postdata(); ?>

