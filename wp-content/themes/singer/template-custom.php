<?php
/**
 * Template Name: Custom Template
 */
?>


<div class="slider">
    <?php
        echo do_shortcode("[metaslider id=6]");
    ?>
</div>

<div class="categories clearfix">
    <div class="row">
        <?php
            $pages = get_pages();

            foreach ($pages as $page) {
                $inHome = get_field('in_home', $page->ID);
                if($inHome) {
                    $categories[get_field('position', $page->ID)] = $page;
                }
            }

            ksort($categories);

            $i = 0;
            foreach ($categories as $category):

        ?>

        <div class="home-category col-md-3">
            <a class="link-category" href="<?php echo get_permalink($category); ?>">
                <div class="content" style="background: url('<?php echo get_the_post_thumbnail_url($category); ?>') no-repeat;">
                    <div class="layer">
                        <h2 class="title"><?php echo $category->post_title; ?></h2>
                    </div>
                </div>
            </a>
        </div>
        <?php $i++;endforeach; ?>
    </div>
</div>
