<?php
/**
 * Template Name: Gabarit évènements
 */

    $showMonth = get_query_var('showdate', date('n'));

    $events = get_posts( array(
        'category_name' => 'agenda',
        'posts_per_page' => -1,
    ));

    $byMonth = array();

    foreach ($events as $event) {
        $date = get_post_meta($event->ID, 'agenda-date');
        $dt = \DateTime::createFromFormat('Ymd', $date[0]);

        $byMonth[$dt->format('n')][] = $event;
    }

    $nextMonth = $showMonth+1;

    if($nextMonth > 12) {
        $nextMonth = '1';
    }

    $lastMonth = $showMonth-1;

    if($lastMonth < 1) {
        $lastMonth = '12';
    }

    $months = array(
        '1' => 'Janvier',
        '2' => 'Février',
        '3' => 'Mars',
        '4' => 'Avril',
        '5' => 'Mai',
        '6' => 'Juin',
        '7' => 'Juillet',
        '8' => 'Aout',
        '9' => 'Septembre',
        '10' => 'Octobre',
        '11' => 'Novembre',
        '12' => 'Décembre'
    );

?>
<div class="row">
    <div class="col-md-12">
        <div class="fil">
            <?php
            if(function_exists('bcn_display'))
            {
                bcn_display();
            }
            ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h1>Evènements</h1>
        </div>
        <div class="month-title">
            <div class="row">
                <div class="col-md-1 text-left col-left">
                    <a href="?showdate=<?php echo $lastMonth; ?>"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-10 col-center">
                    <?php echo $months[$showMonth]; ?>
                </div>
                <div class="col-md-1 text-right col-right">
                    <a href="?showdate=<?php echo $nextMonth; ?>"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
        <?php if (count($byMonth[$showMonth]) == 0): ?>
            <div class="alert alert-warning">
                <?php _e('Aucun évènement pour ce mois...', 'sage'); ?>
            </div>
        <?php endif; ?>
        <div class="row">
            <?php if (count($byMonth[$showMonth]) > 0): ?>
            <?php foreach ($byMonth[$showMonth] as $event): ?>
                <div class="col-md-6">
                    <article>
                        <div class="row">
                            <div class="col-md-4">
                                <figure class="post-thumbnail">
                                    <?php echo get_the_post_thumbnail($event) ?>
                                </figure>
                            </div>
                            <div class="col-md-8">
                                <div class="wrapper-entry">
                                    <header>
                                        <h1 class="entry-title"><?php echo $event->post_title; ?></h1>
                                    </header>
                                    <div class="entry-content">
                                        <?php
                                            $content = apply_filters( 'the_content', $event->post_content );
                                            $content = str_replace( ']]>', ']]&gt;', $content );
                                            echo $content;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            <?php endforeach;endif; ?>
            </div>
        </div>
    </div>
</div>

<?php wp_reset_postdata(); ?>