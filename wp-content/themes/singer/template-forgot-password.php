<?php
/*
Template Name: Mot de passe oublié
*/
    use Roots\Sage\Titles;
    global $wpdb, $user_ID;
?>
<div class="row">
    <div class="col-md-12">
        <div class="fil">
            <?php
            if(function_exists('bcn_display'))
            {
                bcn_display();
            }
            ?>
        </div>
    </div>
</div>
<div class="page-header">
    <h1 class="page-title"><?= Titles\title(); ?></h1>
</div>
<?php
    $newPass = false;
    if(isset($_GET['key']) && $_GET['action'] == "reset_pwd") {

        $reset_key = $_GET['key'];
        $user_login = $_GET['login'];
        $user_data = $wpdb->get_row($wpdb->prepare("SELECT ID, user_login, user_email FROM $wpdb->users WHERE user_activation_key = %s AND user_login = %s", $reset_key, $user_login));

        $user_login = $user_data->user_login;
        $user_email = $user_data->user_email;

        if(!empty($reset_key) && !empty($user_data)) {
            $new_password = wp_generate_password(7, false);

            wp_set_password( $new_password, $user_data->ID );
            //mailing reset details to the user

            $body = '
                                <table>
                    <tr>
                        <td>
                            <img src="http://www.singerfrance.com/wp-content/themes/singer/assets/images/logo-mail-200.png" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Bonjour,
                            <br /><br />
                            <p>
                                Voici votre nouveau mot de passe sur notre site <a href="http://www.singerfrance.com">singerfrance.com</a> a bien été prise en compte.
                            </p>
                            <p>
                                <ul>
                                    <li>Votre email: '.$user_login.'</li>
                                    <li>Votre mot de passe pour vous connecter: '.$new_password.'</li>
                                </ul>
                            </p>
                            <p>En cas de perte de votre mot de passe, utilisez ce lien : <a href="'.wp_lostpassword_url().'">mot de passe perdu.</a></p>
                            <p>A très bientôt sur <a href="http://www.singerfrance.com">singerfrance.com</a> !</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="margin-top:30px;">Suivez nos actualités et nos offres promotionnelles !</p>
                            <a href="https://www.facebook.com/SingerFR/"><img src="http://www.singerfrance.com/wp-content/themes/singer/assets/images/ico-fb.jpg" /></a> <a href="https://www.instagram.com/singerfrance/"><img src="http://www.singerfrance.com/wp-content/themes/singer/assets/images/icon-insta.jpg" /></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="margin:O;">---------------</p>
                            Cet e-mail vous est envoyé automatiquement du serveur <a href="http://www.singerfrance.com">singerfrance.com</a>, merci de ne pas y répondre.
                        </td>
                    </tr>
                </table>
                ';

            wp_mail($user_email, 'Votre nouveau mot de passe sur Singerfrance.com', $body);

            $newPass = true;
        }
    }
?>
<?php if (!$newPass): ?>
<div class="row">
    <div class="col-md-6">
        <p>Afin de retrouver votre mot de passe, veuillez renseigner votre adresse email dans le champs ci-dessous.</p>
        <form class="user_form" id="wp_pass_reset" action="/forget-password.php" method="post">
            <div class="form-group">
            <input id="user_input" type="text" class="form-control" name="user_input" value="" placeholder="Entrez votre adresse E-mail"/>
            </div>
            <input type="hidden" name="action" value="tg_pwd_reset" />
            <input type="hidden" name="tg_pwd_nonce" value="<?php echo wp_create_nonce("tg_pwd_nonce"); ?>" />


            <div class="row">
                <div class="col-lg-6 col-lg-push-3">
                <input type="submit" id="submitbtn" class="btn btn-primary btn-lg btn-block" name="submit" value="Valider" />
                    </div>
            </div>
        </form>
        <div id="result"></div> <!-- To hold validation results -->
    </div>
</div>
<?php else: ?>
<div class="row">
    <div class="col-md-6">
        <p>
            Un email vous a été envoyé à l'adresse <strong><?php echo $user_email; ?></strong> avec votre nouveau mot de passe !
        </p>
    </div>
</div>
<?php endif; ?>
