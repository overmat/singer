<?php
/**
 * Template Name: Gabarit marque
 */
?>
<div class="row">
    <div class="col-md-12">
        <div class="fil">
            <?php
            if(function_exists('bcn_display'))
            {
                bcn_display();
            }
            ?>
        </div>
        <div class="page-header">
            <h1 class="page-title">SINGER, un savoir-faire, une histoire</h1>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="page-content">
            <?php
                $post = get_post();
                $content = apply_filters( 'the_content', $post->post_content );
                $content = str_replace( ']]>', ']]&gt;', $content );
                echo $content;
            ?>
        </div>
    </div>
</div>