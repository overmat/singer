<?php
/**
 * Template Name: Gabarit nos produits
 */
?>
<div class="categories">


<?php

    $terms = get_terms( array(
        'taxonomy' => 'catalogue',
        'hide_empty' => false,
    ) );
    $i= 0;

    foreach($terms as $category):
        if($category->parent == 0):

            if($category->name != 'Accessoires') {
            $metas = get_all_wp_terms_meta($category->term_id);
            $attachment_id = fjarrett_get_attachment_id_by_url($metas['image'][0]);
            $imageInfo = wp_get_attachment_image_src($attachment_id, 'original');

?>
            <div class="product-category home-category <?php if($i%4 == 0):?>first<?php endif; ?>">
                <a class="link-category" href="<?php echo get_term_link($category); ?>">
                    <div class="content" style="background: url('<?php echo $imageInfo[0]; ?>') no-repeat center;">
                        <div class="layer">
                            <h2 class="title"><?php echo $category->name; ?></h2>
                        </div>
                    </div>
                </a>
            </div>
<?php
        $i++;
        } else {
                $accesoires = $category;
                $metas = get_all_wp_terms_meta($category->term_id);
                $attachment_id = fjarrett_get_attachment_id_by_url($metas['image'][0]);
                $imageInfoAccess = wp_get_attachment_image_src($attachment_id, 'original');
            }
    endif;
    endforeach;
?>
    <div class="home-category accessoires">
        <a class="link-category" href="<?php echo get_term_link($accesoires); ?>">
            <div class="content" style="background: url('<?php echo $imageInfoAccess[0]; ?>') no-repeat center;">
                <div class="layer">
                    <h2 class="title"><?php echo $accesoires->name; ?></h2>
                </div>
            </div>
        </a>
    </div>
</div>
<?php if($empty): ?>
    <div class="alert alert-warning">
        <?php _e('Aucun produit pour le moment...', 'sage'); ?>
    </div>
<?php endif; ?>
