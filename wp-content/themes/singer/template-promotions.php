<?php
/**
 * Template Name: Gabarit promotions
 */
?>
<?php
query_posts( array('post_type' => 'promotion', 'post_per_page' => -1) );
?>
<div class="row">
    <div class="col-md-12">
        <div class="fil">
            <?php
            if(function_exists('bcn_display'))
            {
                bcn_display();
            }
            ?>
        </div>
    </div>
</div>
    <div class="page-header">
        <h1>Promotions</h1>
    </div>
<?php if (!have_posts()) : ?>
    <div class="alert alert-warning">
        <?php _e('Aucune promotion en cours', 'sage'); ?>
    </div>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
    <div <?php post_class(); ?>>
        <div class="row">
            <div class="col-md-12 image">
                <?php if (get_field('wpcf-url-promo') !== false): ?>
                <a href="<?php echo get_field('wpcf-url-promo');?>" target="_blank">
                    <?php endif; ?>
                <figure class="post-thumbnail">
                    <?php the_post_thumbnail(); ?>
                    <div class="layer">
                        <?php setlocale(LC_ALL, 'fr_FR'); ?>
                        <span class="promo-title"><?php the_title(); ?></span><span class="dates">Du <?php echo strftime('%d %B', get_field('wpcf-date-debut')); ?> au <?php echo strftime('%d %B', get_field('wpcf-date-fin')); ?></span>
                    </div>
                </figure>
                    <?php if (get_field('wpcf-url-promo') !== false): ?>
                </a>
            <?php endif; ?>
            </div>
        </div>
    </div>
<?php endwhile; ?>

<?php wp_reset_postdata(); ?>