<?php
/**
 * Template Name: Gabarit vidéos et tutos
 */
?>
<div class="categorie-video-tutos">
    <div class="row">
        <div class="col-md-12">
            <div class="fil">
                <!-- Breadcrumb NavXT 5.7.0 -->
                <span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to Singer France." href="/" class="home"><span property="name">Singer France</span></a><meta property="position" content="1"></span> &gt; <span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to Vidéos et tutos." href="https://www.singerfrance.com/videos-et-tutos/" class="post post-tuto-archive"><span property="name">Vidéos et tutos</span></a> </div>
        </div>
    </div>
    <div class="page-header">
        <h1>Vidéos et tutos</h1>
    </div>
    <?php

    $terms = get_terms( array(
        'taxonomy' => 'categories-tuto',
        'hide_empty' => false,
    ) );
    $empty = true;
    foreach($terms as $category):
        if($category->parent == 0):
            $metas = get_all_wp_terms_meta($category->term_id);
            $attachment_id = fjarrett_get_attachment_id_by_url($metas['image-cat-tuto'][0]);
            $imageInfo = wp_get_attachment_image_src($attachment_id, 'original');
            $empty = false;
            ?>
            <div class="tuto-category">
                <a class="link-category" href="<?php echo get_term_link($category); ?>">
                    <div class="content" style="background: url('<?php echo $imageInfo[0]; ?>') no-repeat center;-webkit-background-size: cover;background-size: cover;">
                        <div class="layer">
                            <h2 class="title"><?php echo $category->name; ?></h2>
                        </div>
                    </div>
                </a>
            </div>
            <?php
        endif;
    endforeach;
    ?>
</div>
<?php if($empty): ?>
    <div class="alert alert-warning">
        <?php _e('Aucun produit pour le moment...', 'sage'); ?>
    </div>
<?php endif; ?>

