<div class="row">
    <div class="col-md-12">
        <div class="fil">
            <?php
            if(function_exists('bcn_display'))
            {
                bcn_display();
            }
            ?>
        </div>
    </div>
</div>

<div class="page-header">
    <div class="page-title">Vidéos et tutos</div>
</div>
<?php while (have_posts()) : the_post(); ?>
    <article <?php post_class(); ?>>
        <div class="clearfix article-wrapper">
            <div class="information">
                <h1 class="entry-title"><?php the_title(); ?></h1>
                <p class="subtitle"><?php echo get_field('wpcf-sous-titre'); ?></p>

                <p class="level">Niveau :
                    <?php for($i=0;$i< (int)get_field('wpcf-niveau');$i++): ?>
                        <img class="img-star" src="<?php echo get_template_directory_uri(); ?>/assets/images/star-black.jpg" />
                    <?php endfor; ?>
                    <?php for($i=0;$i< 3 -(int)get_field('wpcf-niveau');$i++): ?>
                        <img class="img-star" src="<?php echo get_template_directory_uri(); ?>/assets/images/star-white.jpg" />
                    <?php endfor; ?>
                </p>

                <p class="duration">
                    Durée : <?php echo get_field('wpcf-duree'); ?>
                </p>

                <div class="fourniture">Fournitures : </div>
                <ul class="fournitures-list">
                    <?php
                        $metas = get_post_meta(get_post()->ID);
                    ?>
                    <?php foreach($metas['wpcf-fourniture'] as $fourniture): ?>
                        <li>- <?php echo $fourniture; ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="entry-content">
                <?php
                    $imageDescription = get_field('wpcf-image-description');
                    if($imageDescription != null && $imageDescription !== false) {
                ?>
                        <img src="<?php echo $imageDescription;?>" alt="<?php the_title(); ?>"/>
                <?php
                    }

                ?>
            </div>
        </div>
        <div class="content-wrapper">
            <?php the_content(); ?>
        </div>
        <div class="detail-tuto">
            <?php
            for($i=1;$i<=50;$i++):

                $image1 = get_field('wpcf-image-tuto-'.$i);
                $description1 = get_field('wpcf-image-texte-'.$i);
                $align = get_field('wpcf-alignement-'.$i);

                if($image1 !== '' && $image1 !== false):
            ?>
            <div class="bloc-info">
                <div class="row">
                    <div class="col-md-6">
                        <?php if($align == 'left'): ?>
                            <img class="img-detail-tuto" src="<?php echo $image1;?>" />
                        <?php else: ?>
                            <div class="description-right">
                                <h2 class="tuto-step-title">Etape <?php echo $i; ?></h2>
                                <?php echo $description1; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-6">
                        <?php if($align == 'left'): ?>
                            <div class="description-left">
                                <h2 class="tuto-step-title">Etape <?php echo $i; ?></h2>
                                <?php echo $description1; ?>
                            </div>
                        <?php else: ?>
                            <img class="img-detail-tuto" src="<?php echo $image1; ?>" />
                        <?php endif; ?>
                    </div>
                </div>
            </div>
                    <?php
                        $imageNext = get_field('wpcf-image-tuto-'.($i+1));
                    if($imageNext !== '' && $imageNext !== false): ?>
            <div class="separator-tuto-step"></div>
            <?php endif;endif;endfor; ?>
        </div>
    </article>
<?php endwhile; ?>
