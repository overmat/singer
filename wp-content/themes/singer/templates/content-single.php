<div class="row">
    <div class="col-md-12">
        <div class="fil">
            <?php
            if(function_exists('bcn_display'))
            {
                bcn_display();
            }
            ?>
        </div>
        <div class="page-header">
            <h1 class="page-title"><?php the_title(); ?></h1>
        </div>
    </div>
</div>
<?php while (have_posts()) : the_post(); ?>
  <div <?php post_class(); ?>>
      <figure class="post-thumbnail">
          <?php the_post_thumbnail(); ?>
      </figure>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
  </div>
<?php endwhile; ?>
