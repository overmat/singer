<a class="link-to-post" href="<?php the_permalink(); ?>">
    <article <?php post_class(); ?>>
      <header class="post-header clearfix">
        <h2 class="entry-title"><?php the_title(); ?></h2>
      </header>
        <figure class="post-thumbnail">
            <?php the_post_thumbnail('actu-thumbnail'); ?>
        </figure>
      <div class="entry-summary">
        <?php the_excerpt(); ?>
      </div>
    </article>
</a>