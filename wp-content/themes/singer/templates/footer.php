<footer class="content-info">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="informations">
                    <p class="displayed-text">
                        Singer, la marque de référence en matière de couture : 165 ans de savoir-faire et de notoriété grâce à la qualité, la technologie et la longévité de ses machines à coudre.<br />
                        Notre marque propose aujourd’hui une large gamme de machines à coudre, brodeuses, surjeteuses et accessoires couture.
                    </p>
                    <p id="about-text">
                        Singer bénéficie surtout d’une organisation constituée de professionnels au service de la créativité et du confort de leurs utilisatrices couvrant la quasi-totalité du territoire français. Conseils, démonstrations, formations ou encore cours de couture sont autant d’atouts qui participent à la qualité du service de l’enseigne.<br />
                        Imaginez, créez, recyclez, customisez sans limite avec les machines à coudre SINGER : « Elles ont la technique, vous avez du talent ! »
                    </p>
                    <a href="#" id="about-more" class="more-link" data-open="false">
                        <div class="more">
                            En savoir plus
                            <i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="main-footer-nav">
                    <?php wp_nav_menu( array( 'theme_location' => 'Footer_2' ) ); ?>
                </div>
            </div>
            <div class="products-footer">
                <h4 class="product-title ">Nos produits</h4>
                <div class="secondary-footer-nav">
                    <?php wp_nav_menu( array( 'theme_location' => 'Footer_1' ) ); ?>
                </div>
            </div>
            <div class="social-footer">
                <div class="menu-action-footer">
                    <div class="fb-page" data-href="https://www.facebook.com/SingerFR/" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/SingerFR/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/SingerFR/">SingerFR</a></blockquote></div>
                    <a href="https://www.instagram.com/singerfrance/" target="_blank">
                        <img class="block-insta" src="<?php echo get_template_directory_uri();?>/assets/images/Insta_Singer.png">
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="menu-action-terms clearfix">
                    <div class="row">
                        <div class="col-md-7 col-md-push-1 text-center col-sm-12 newsletter-content">
                            <p class="baseline">Tenez-vous informé de nos actualités en souscrivant à notre <span class="news">newsletter</span></p>
                        </div>
                        <div class="col-md-4 col-md-push-1 col-sm-12 newsletter-form">
                            <?php echo do_shortcode('[mc4wp_form id="878"]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="legal"><a href="<?php echo get_permalink(85); ?>">Mentions légales / CGV</a> © 2017 Singer France. Tous droits réservés   -  Site créé par <a target="_blank" href="http://www.josianefaitdelapub.com">josiane</a></p>
            </div>
        </div>
    </div>
</footer>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-52516091-1', 'auto');
    ga('send', 'pageview');

</script>
