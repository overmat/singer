<div id='slider-menu'>
    <nav id="navigation1" class="navigation clearfix hidden-xs">
        <?php wp_nav_menu( array( 'theme_location' => 'Main' ) ); ?>
        <ul>
            <li id="menu-item-account" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-account">Espace client
                <ul class="sub-menu">
                    <?php if(is_user_logged_in()): ?>
                        <li id="menu-item-262" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-262"><a href="<?php echo esc_url(home_url('mon-compte')); ?>">Mon compte</a></li>
                        <li id="menu-item-264" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-264"><a href="<?php echo esc_url(home_url('mon-compte/extension-de-garantie')); ?>">Extension de garantie</a></li>
                        <li id="menu-item-267" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-267"><a href="<?php echo wp_logout_url(); ?>">Déconnexion</a></li>
                    <?php else: ?>
                        <li id="menu-item-262" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-262"><a href="<?php echo esc_url( site_url( 'wp-login.php', 'login_post' ) ); ?>">Se connecter</a></li>
                        <li id="menu-item-264" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-264"><a href="<?php echo esc_url(home_url( 'inscription' )); ?>">S'inscrire</a></li>
                    <?php endif; ?>
                </ul>
            </li>
        </ul>
    </nav>
</div>
<div id="nav-icon3">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
</div>
<header class="banner">
  <div class="container">
      <div class="row">
          <div class="col-md-3 col-sm-12 logo-wrapper">
              <a class="brand" href="<?= esc_url(home_url('/')); ?>">
                  <img class="logo" src="<?php echo get_template_directory_uri();?>/assets/images/logo.jpg" alt="<?php echo get_bloginfo('description'); ?>">
              </a>
          </div>
          <div class="col-md-9 col-sm-12">
              <nav id="navigation1" class="navigation clearfix hidden-xs">
                  <?php wp_nav_menu( array( 'theme_location' => 'Main' ) ); ?>
                  <div class="social-network">
                      <a href="https://www.instagram.com/singerfrance/" target="_blank">
                          <img class="social" src="<?php echo get_template_directory_uri();?>/assets/images/icon-insta.jpg" alt="Instagram">
                      </a>
                      <a target="_blank" href="https://www.facebook.com/SingerFR/">
                          <img class="social fb" src="<?php echo get_template_directory_uri();?>/assets/images/ico-fb.jpg" alt="Facebook">
                      </a>
                  </div>
                  <ul class="nav-account">
                      <li id="menu-item-account" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-account">
                          <?php
                                if(is_user_logged_in()):
                                    $userInfo = wp_get_current_user();
                            ?>
                            Bonjour <?php echo ucfirst($userInfo->user_firstname); ?>
                          <?php else: ?>
                                Espace client
                          <?php endif; ?>
                          <ul class="sub-menu">
                              <?php if(is_user_logged_in()): ?>
                              <li id="menu-item-262" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-262"><a href="<?php echo esc_url(home_url('mon-compte')); ?>">Mon compte</a></li>
                              <li id="menu-item-264" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-264"><a href="<?php echo esc_url(home_url('mon-compte/extension-de-garantie')); ?>">Extension de garantie</a></li>
                              <li id="menu-item-267" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-267"><a href="<?php echo wp_logout_url(); ?>">Déconnexion</a></li>
                              <?php else: ?>
                                <li id="menu-item-262" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-262"><a href="<?php echo esc_url( site_url( 'wp-login.php', 'login_post' ) ); ?>">Se connecter</a></li>
                                <li id="menu-item-264" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-264"><a href="<?php echo esc_url(home_url( 'inscription' )); ?>">S'inscrire</a></li>
                              <?php endif; ?>
                          </ul>
                      </li>
                  </ul>
              </nav>
          </div>
      </div>
  </div>
</header>