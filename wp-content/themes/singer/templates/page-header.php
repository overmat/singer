<?php use Roots\Sage\Titles; ?>
<div class="row">
    <div class="col-md-12">
        <div class="fil">
            <?php
            if(function_exists('bcn_display'))
            {
                bcn_display();
            }
            ?>
        </div>
    </div>
</div>
<div class="page-header">
  <h1 class="page-title"><?= Titles\title(); ?></h1>
</div>
