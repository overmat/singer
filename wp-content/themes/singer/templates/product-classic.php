<div class="row">
    <div class="col-md-12">
        <div class="fil">
            <?php
            if(function_exists('bcn_display'))
            {
                bcn_display();
            }
            ?>
        </div>
    </div>
</div>
<div class="row classic-product-page">
    <div class="col-md-5 product-image-wrapper">
        <?php
            $metas = get_post_meta($post->ID);

            $productImages = $metas['wpcf-image-produit'];
        ?>


            <ul id="light-slider">
                <?php foreach ($productImages as $image): ?>
                    <li data-thumb="<?php echo $image;?>">
                        <img src="<?php echo $image;?>" />
                    </li>
                <?php endforeach; ?>
            </ul>

    </div>
    <div class="col-md-6 col-md-push-1 product-information-wrapper">
        <div class="product-wrapper">
            <h1 class="title">
                <?php the_title(); ?>
            </h1>

            <div class="description">
                <?php
                $content = apply_filters( 'the_content', $post->post_content );
                $content = str_replace( ']]>', ']]&gt;', $content );
                echo $content;
                ?>
            </div>

            <p class="price">
                Prix indicatif conseillé : <strong><?php echo get_field('wpcf-prix-produit'); ?>&euro;</strong>
            </p>
            <?php if(get_field('wpcf-video') !== '' && get_field('wpcf-video') !== false): ?>
                <a data-fancybox class="btn-video btn-red" href="<?php echo get_field('wpcf-video'); ?>">Voir la vidéo</a>
            <?php endif; ?>
            <?php if(get_field('wpcf-dispo-mag') == '1'): ?>
                <a class="btn-video btn-red" target="_blank" href="<?php echo get_permalink(13); ?>">Disponible en magasin</a>
            <?php endif; ?>
        </div>
    </div>
</div>