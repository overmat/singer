<div class="row">
    <div class="col-md-12">
        <div class="fil">
            <?php
            if(function_exists('bcn_display'))
            {
                bcn_display();
            }
            ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="product-wrapper product-gamme">
            <img class="image-product-large" src="<?php echo get_field('wpcf-image-produit');?>" />
            <div class="product-left">
                <h1 class="title"><?php the_title(); ?></h1>
                <div class="description">
                    <?php
                    $content = apply_filters( 'the_content', $post->post_content );
                    $content = str_replace( ']]>', ']]&gt;', $content );
                    echo $content;
                    ?>
                </div>
                <p class="price">
                    Prix indicatif conseillé : <strong><?php echo get_field('wpcf-prix-produit'); ?>&euro;</strong>
                </p>
                <?php if(get_field('wpcf-video') !== '' && get_field('wpcf-video') !== false): ?>
                    <a data-fancybox class="btn-video btn-red" href="<?php echo get_field('wpcf-video'); ?>">Voir la vidéo</a>
                <?php endif; ?>
                <?php if(get_field('wpcf-dispo-mag') == '1'): ?>
                    <a class="btn-video btn-red" target="_blank" href="<?php echo get_permalink(13); ?>">Disponible en magasin</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>