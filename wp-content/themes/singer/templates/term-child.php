<?php
    $metas = get_all_wp_terms_meta($term->term_id);

    if(isset($metas['image'])):
        $attachment_id = fjarrett_get_attachment_id_by_url($metas['image'][0]);
        $imageInfo = wp_get_attachment_image_src($attachment_id, 'gamme-vignette');
?>
<div class="term-catalogue">
    <div class="row">
        <div class="col-md-12">
            <a href="<?php echo get_term_link($term); ?>" class="term-link">
                <div class="image-wrapper">
                    <img src="<?php echo $imageInfo[0]; ?>" />
                    <div class="term-info">
                        <div class="title"><?php echo $term->name; ?></div>
                        <div class="description"><?php echo $term->description; ?></div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>

<?php endif; ?>