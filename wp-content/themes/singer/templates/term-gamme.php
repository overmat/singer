<?php
query_posts( array('post_type' => 'produit', 'posts_per_page' => 100000,'tax_query' => array(
    array(
        'taxonomy' => 'catalogue',
        'field' => 'term_id',
        'terms' => $termMain->term_id,
    )
)) );


?>
<div class="row">
    <div class="col-md-12">
        <div class="fil">
            <?php
            if(function_exists('bcn_display'))
            {
                bcn_display();
            }
            ?>
        </div>
    </div>
</div>
<div class="row">
    <?php while (have_posts()) : the_post(); ?>
        <?php $empty = false; ?>
        <div class="col-md-6 post-product-list">
            <a href="<?php the_permalink(); ?>">
                <article <?php post_class(); ?>>
                    <?php the_post_thumbnail(); ?>
                    <div class="wrapper-info">
                        <div class="title-description">
                            <h1 class="title"><?php the_title(); ?></h1>
                            <div class="description-short">
                                <?php echo get_field('wpcf-description-courte'); ?>
                            </div>
                        </div>
                        <div class="cta">
                            Je découvre +
                        </div>
                    </div>
                </article>
            </a>
        </div>
    <?php endwhile; ?>
</div>

<?php wp_reset_postdata(); ?>

