<?php
query_posts( array('post_type' => 'produit','posts_per_page' => 100000, 'tax_query' => array(
    array(
        'taxonomy' => 'catalogue',
        'field' => 'term_id',
        'terms' => $termMain->term_id
    )
)) );

$metas = get_all_wp_terms_meta($termMain->term_id);

?>
<div class="row">
    <div class="col-md-12">
        <div class="fil">
            <?php
            if(function_exists('bcn_display'))
            {
                bcn_display();
            }
            ?>
        </div>
    </div>
</div>
<div class="page-header">
    <h1 class="page-title"><?php echo $termMain->name; ?></h1>
</div>
<?php
    if(isset($metas['bandeau-listing'])):
        $attachment_id = fjarrett_get_attachment_id_by_url($metas['bandeau-listing'][0]);
        $imageInfo = wp_get_attachment_image_src($attachment_id, 'original');
?>
    <div class="row">
        <div class="col-md-12">
            <div class="banner-list-product">
                <img src="<?php echo $imageInfo[0]; ?>" />
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <?php while (have_posts()) : the_post(); ?>
        <?php $empty = false; ?>
        <div class="post-product-list <?php if(isset($metasMain['gamme']) && $metasMain['gamme'][0] == 'checked'): ?>col-md-4 classic-list<?php else:?>col-md-4 classic-list<?php endif; ?> ">
            <a class="link-to-post" href="<?php the_permalink(); ?>">
                <article <?php post_class(); ?>>
                    <?php the_post_thumbnail(); ?>
                    <div class="wrapper-info">
                        <div class="title-description">
                            <h1 class="title"><?php the_title(); ?></h1>
                            <div class="description-short">
                                <?php echo get_field('wpcf-description-courte'); ?>
                            </div>
                            <div class="price">
                                Prix : <?php echo get_field('wpcf-prix-produit'); ?> €
                            </div>
                        </div>
                        <div class="cta">
                            Je découvre +
                        </div>
                    </div>
                </article>
            </a>
        </div>
    <?php endwhile; ?>
</div>
<?php wp_reset_postdata(); ?>

