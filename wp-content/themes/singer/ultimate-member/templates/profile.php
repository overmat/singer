<?php $metas = get_user_meta(get_current_user_id()); ?>
<div class="container um-profile-page">
    <div class="row">
        <div class="col-md-6 col-md-push-3">
            <div class="row">
                <div class="col-md-6">
                    <a class="btn-account" href="http://srv.site/singer/account/general/">Modifier mon compte</a>
                </div>
                <div class="col-md-6">
                    <a class="btn-account" href="http://srv.site/singer/account/password/">Changer mon mot de passe</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper-account">
                        <h2>Informations de mon compte</h2>
                        <div class="content">
                            <div class="info-group">
                                <div class="label-profile">Nom d'utilisateur : </div>
                                <div class="value-profile"><?php echo $metas['user_login'][0]; ?></div>
                            </div>
                            <div class="info-group">
                                <div class="label-profile">Adresse email :</div>
                                <div class="value-profile"><?php echo get_userdata(get_current_user_id())->user_email;?></div>
                            </div>
                        </div>
                        <h2>Adresse postale</h2>
                        <div class="content">
                            <div class="info-group">
                                <div class="label-profile">Prénom :</div>
                                <div class="value-profile"><?php echo get_userdata(get_current_user_id())->first_name;?></div>
                            </div>
                            <div class="info-group">
                                <div class="label-profile">Nom :</div>
                                <div class="value-profile"><?php echo get_userdata(get_current_user_id())->last_name;?></div>
                            </div>
                            <div class="info-group">
                                <div class="label-profile">Address :</div>
                                <div class="value-profile"><?php echo get_user_meta(get_current_user_id(), 'user_adress', true);?><br /><?php echo get_user_meta(get_current_user_id(), 'zip_code', true);?> <?php echo get_user_meta(get_current_user_id(), 'city', true);?></div>
                            </div>
                            <div class="info-group">
                                <div class="label-profile">Téléphone :</div>
                                <div class="value-profile"><?php echo get_user_meta(get_current_user_id(), 'phone_number', true);?></div>
                            </div>
                        </div>
                        <h2>Extension de garantie</h2>
                        <div class="content">
                            <?php if(get_user_meta(get_current_user_id(), 'model_a', true) == ''): ?>
                            <p>Vous ne bénéficiez pas de la garantie étendue</p>
                            <a href="">Profiter de ma garantie étendue</a>
                            <?php else: ?>
                            <p>Votre modèle <strong><?php echo get_user_meta(get_current_user_id(), 'model_a', true); ?></strong> bénéficie de la garantie étendue.</p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>